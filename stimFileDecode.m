function[] = stimFileDecode(swFile)
%region6 is C00
stripZeroOffset = 'C00';

%region7 is E00
%stripZeroOffset = 'E00'

if(~exist('swFile', 'var'))
    swFile = 'packetDataIn_v1.txt';
end


[tpData tpAddr] = textread(swFile, '%s %s');
tpAddr = hex2dec(cell2mat(tpAddr(1:end-1)));
tpData
tpAddr
%select out fibers
%tpData0 = tpData(find(tpAddr == 32),:);
tpData = cell2mat(reshape(tpData(1:end-1),4,[])');
tpAddr = tpAddr(1:4:end);
nTp0 = size(tpData);
for i = 1:nTp0

    [nHitsTp,bcidTp,vmmIdTp,artDataTp] = addcPacketDecode(tpData(i,:));
    fifoAddr = tpAddr(i);

    disp(['hit index:',num2str(i),' FIFO Address:', dec2hex(fifoAddr), ' bxid: ',num2str(bcidTp),'----------']);
    nHitsTp;        
    vmmIdTp;
    vmmIdTpBin = dec2bin(hex2dec(vmmIdTp),5);
    planeTp = dec2hex(bin2dec(vmmIdTpBin(:,1:2)) + repmat(mod(fifoAddr,2)*4,nHitsTp,1));
    vmmTp = bin2dec(vmmIdTpBin(:,3:5));
    artDataTp ; 
    
    stripNumber=dec2hex(vmmTp .*2^6 + hex2dec(artDataTp) + ...
                        repmat(hex2dec(stripZeroOffset),nHitsTp,1),4);
    
    disp(flipud([planeTp,repmat('-',nHitsTp,1), stripNumber]))
end