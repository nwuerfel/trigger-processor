function[signed] = convertUnsToSign(unsigned, nOfBits, shift)
	unshifted = unsigned.*(2^(-shift));
	wrapped   = mod(unshifted,(2^(nOfBits)));
	converted = wrapped - (wrapped >= 2^(nOfBits-1))*(2^(nOfBits));
	signed = converted.*(2^(shift));
end