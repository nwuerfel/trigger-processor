%% artWritePackets: extract sequence of 32-bit words from a packet input file
%% and write them to the fifo via tcp;
%% a new tcp packet is sent for each particular input fifo
function artPgWrite(tcpObj,inFile)
	header = 'abcd1234';
	type = 'FE170002';
disp('here')
	maxChunksPerWrite = 100; % limit the number of chunks sent in one tcp packet

	[hexChunks, hexAddr] = textread(inFile, '%s %s');
	chunks = hex2dec(hexChunks)
	numAddr = hex2dec(hexAddr)
	for fifo = [33 0 34] % offset by 32 to put in correct address space
		chunksToSend = chunks(find(numAddr(:,1) == fifo));
		if(~isempty(chunksToSend))
			addr = ['000000',dec2hex(fifo,2)]

				tcpPayload = [hex2dec([header;type;addr]);chunksToSend]
dec2hex(tcpPayload)
				 hexPayload = dec2hex(tcpPayload,8)
				 fwrite(tcpObj, tcpPayload, 'uint32');

				%% wait a while to allow the tcp packet to be received
				pause(.3);
			
		end
	end
	
end
