function [swResultsPass swResultsFail] = keepCommonOnly(swResults,hwResults,bcidHW)
    swResultsPass = [];
    swResultsFail = [];
    currentHwIndex = 1; %we know there are repetitions, so will only check a few HW events
    for i=1:size(swResults,1)
        bcidSW = swResults(i,13:20);
        if size(find(bcidSW==bcidHW(currentHwIndex)),2)>0 | size(find(bcidSW+1==bcidHW(currentHwIndex)),2)>0
            swResultsPass = [swResultsPass; [swResults(i,:) i]];
            currentHwIndex = currentHwIndex+1;
        else
            swResultsFail = [swResultsFail; [swResults(i,:) i]];
        end
        if currentHwIndex>size(bcidHW,1)
            return;
        end
    end
end
    
