function[reslist] = testSubset(subset, label)
	dataInLabel = strcat(label, '.csv');
	fifoOutLabel = strcat(label, '-fifoOut.csv');
	resListLabel = strcat(label, '-reslist.mat');

	if(~isvector(subset))
		disp('Invalid input');
		return;
	elseif(iscolumn(subset))
		subset = subset';
	end

	subset = sort(subset);

	reslist = [subset'];

	subsetHits = hitsForEvents(subset);

	mat2csvF(subsetHits, dataInLabel);
	fifoData = doItF(dataInLabel);
	save('lastFifoOut.mat', 'fifoData');
	dlmwrite(fifoOutLabel, fifoData, '');
	
	harRes = formatTPDebug(fifoData);

	mat2res(subsetHits);
	load('HitResults');
	% simRes = reorderMat2Res(subset, HitResults);
	simRes = HitResults;

	reslist(:,2:32) = harRes;
	reslist(:,33:39) = simRes;

	save(resListLabel, 'reslist');
end

