%% doPackets: given a packet input file (as produced by 'pack2sim'), write the packets
%% to the hardware fifo, run the hardware and return the hexadecimal output;
%% the output is also saved in the file 'packetDataOut.csv'; a second optional argument
%% can be given to specify a different filename for it.
function [output] = getPackets(inFile, outFile)
    useParUdp = 0;

    if(~exist('outfile', 'var'))
        outFile = 'packetDataOut.csv';
    end
    if(~exist('inFile', 'var'))
        inFile = 'packetDataIn_v1.txt';
    end
    if useParUdp
        disp('Opening UDP Receiver as a parallel  process')
        job = batch('udpRecDsp')
        pause(5)
    end
    disp('Opening TCP connection')
    t = tcpOpenMmtp; 
    disp('Reset debug FIFOs and BC counter')
    bxidReset(t)
    disp(['FIFO Status : ',regRead(t,'00000001')]);
    disp('Writing ART data to Emulator')
    artWritePackets(t, inFile);
    disp('Starting DAQ')
    %    startDaq(t)
    pause(1)
    %    disp('Stopping DAQ')
    stopDaq(t);
    disp(['FIFO Status : ',regRead(t,'00000001')]);
    tcpCloseMmtp(t)
    if useParUdp
        wait(job)
        delete(job)
        clear job
    end
    showResults
    %	output = fifoData(2:end,:); % strip header for concatenation
end