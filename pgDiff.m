function[] = pgDiff(pgFile,swFile)

nAddc = 1
disp(['input data is expects ', num2str(nAddc),' ADDCs']);

if(~exist('pgFile', 'var'))
    pgFile = 'mmTest21.txt'
end

if(~exist('swFile', 'var'))
    swFile = 'packetDataIn_v1.txt';
end

pgDataIn = cell2mat(textread(pgFile, '%s'));
spacer = repmat(' ',size(pgDataIn,1),1);
pgData = reshape(pgDataIn',32,[])';
if nAddc == 1
    pgData = pgData(2:2:end,:);
end


[tpData tpAddr] = textread(swFile, '%s %s');
tpAddr = hex2dec(cell2mat(tpAddr));
tpData0 = tpData(find(tpAddr == 32),:);
tpData0 = cell2mat(reshape(tpData0,4,[])');
nError = 0;

%pgData(9:10,:) = [];

testRange = 1:10;
pgData(testRange,:)
tpData0(testRange,:)

nPg = length(pgData);
nTp0 = length(tpData0);
for i = testRange % 1:min(nPg,nTp0)
    if nAddc == 1
        [nHitsPg,bcidPg,vmmIdPg,artDataPg] = addcPacketDecode(pgData(i,:));
    end
    if nAddc == 2
  disp('here')
        [nHitsPg0,bcidPg0,vmmIdPg0,artDataPg0] = addcPacketDecode(pgData(2*i-1,:));        
        [nHitsPg1,bcidPg1,vmmIdPg1,artDataPg1] =  addcPacketDecode(pgData(2*i,:));
        nHitsPg = nHitsPg1 + nHitsPg0;
        bcidPg = bcidPg0;
        vmmIdPg0 = dec2hex(hex2dec(vmmIdPg0)+16,2);
        vmmIdPg = vertcat(vmmIdPg1,vmmIdPg0);
        artDataPg1
        artDataPg0       
        artDataPg = vertcat(artDataPg1,artDataPg0)
    end    

    [nHitsTp,bcidTp,vmmIdTp,artDataTp] = addcPacketDecode(tpData0(i,:));
    mismatch = 0;
    if (nHitsPg == nHitsTp)
        if            (...
            (vmmIdPg == vmmIdTp) ...
            & (artDataPg == artDataTp)...
            )
            disp([num2str(i),'match'])
        else 
            mismatch = 1;            
        end
    else
        mismatch = 1;
    end
    if  mismatch == 1
        nError = nError+1;
        disp(['mismatch on index:', num2str(i)])
        nError
        nHitsPg
        nHitsTp        
        vmmIdPg
        vmmIdTp
        artDataPg 
        artDataTp  
    end        
    disp(['nError:', num2str(nError)])
end
