function[errors] =  checkArtTestPattern(outfile)
if(~exist('outfile', 'var'))
    outfile = 'artTestDataV2Ans'; 
end
debugDataFile = fopen('debugData3.txt');
debugData3 = textscan(debugDataFile,'%s');
fclose(debugDataFile);
debugData3 = cell2mat(debugData3{1,1});
artTestDataAnsFile = fopen([outfile,'.txt']);
artTestDataAns = textscan(artTestDataAnsFile,'%s');
fclose(artTestDataAnsFile);
artTestDataAns = cell2mat(artTestDataAns{1,1})
debugDataY = debugData3(:,17:32);
debugDataM = debugData3(:,49:64);
debugData = [debugDataY,debugDataM]
result = isequal(debugDataY,artTestDataAns);
disp(['BXID hits processed: ', num2str(size(debugData,1))]);
errorMat = debugData == artTestDataAns;
errors = sum(sum(not(errorMat),2));
errorIndex = find(not(errorMat))

if errors == 0
    disp('success! ');
else
    disp(['errors found  ', num2str(errors)]);
    errorMat
    fprintf([debugData(errorIndex'), ' should be \n', artTestDataAns(errorIndex)]);  
end;
 
