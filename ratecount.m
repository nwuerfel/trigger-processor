clear all

hProb = [.25 .25]
N = 32
p = .0084
for k = 0:N
  hits(k+1) =  (factorial(N)/(factorial(k)*factorial(N-k)))*(p^k*(1-p)^(N-k))
end
figure(1)
bar(0:N,hits)
for sInd = 0:N
    accum(sInd+1) = sum(hits(1:sInd+1))
end
figure(2)
bar(0:N,accum)
    