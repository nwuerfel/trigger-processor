%% artWriteRows: given a tcp object and an array originating from a hit data input file,
%% send the hits to the hardware in a tcp packet
function artWriteRows(tcpObj,dataIn)
	header = 'abcd1234';
	type = 'FE170002';
	addr = '00000013';
	hex2dec([header;type;addr]);

	dataIn = dataIn(:,2:4);
	dataIn = reshape(dataIn,[],3);
	dataInDC =  ((dataIn(:,1)-1)*2^16) + (dataIn(:,2)*2^0) + (mod(dataIn(:,3),4096)*2^20);
        disp([dec2hex(dataInDC)])
	tcpPayload = [hex2dec([header;type;addr]);dataInDC];
	fwrite(tcpObj,tcpPayload, 'uint32');

	pause(.3);
end


