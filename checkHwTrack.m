function [trackPossible] = checkHwTrack(hits)
	% verify if a track can be produced given reading of at most 8 hits at given bxid

	planes = hits(:,5);
	bcs = hits(:,10);

	xPlanes = [1, 2, 5, 6];
	uvPlanes = [3, 4, 7, 8];

	prevBc = bcs(1);
	firstBc = bcs(1);
	readIn = 0;
	maxReadIn = 8;
	maxBcDist = 2;
	readPlanes = []; readBxid = [];
	for k = 1:numel(bcs)
		if(bcs(k) == prevBc)
			if(readIn < maxReadIn)
				readIn = readIn + 1;
				readPlanes = [readPlanes; planes(k)];
				readBxid = [readBxid; bcs(k)];
				prevBc = bcs(k);
			end
		elseif(bcs(k)-firstBc <= maxBcDist)
			readIn = 1;
			readPlanes = [readPlanes; planes(k)];
			readBxid = [readBxid; bcs(k)];
			prevBc = bcs(k);
		end
	end
	[readPlanes, readBxid]
	
	trackPossible = (numel(intersect(xPlanes, readPlanes)) >= 3 && ...
				numel(intersect(uvPlanes, readPlanes)) >= 3);
end
				


