%% assemblePackets: given some (Bryan's) Hits, produce a cell array with packet information
%% where each row has the format: {{bxid}, {packets}} and where
%% 'packets' is a cell array of the format {{layer, side}, packetData}}
function [Packets] = assemblePackets(Hits)
	hitsPerPacket = 8;
	bxidCol = 2;
	planeCol = 3;
    stationCol = 4;
	stripCol = 5;

	planesA = [0, 1, 2, 3];
	planesB = [4, 5, 6, 7];
	xPlanes = [0, 1, 6, 7];
	uPlanes = [2, 4];
	vPlanes = [3, 5];

	bxid = Hits(:,bxidCol);
	plane = Hits(:,planeCol);
	[layer, vmmid, art] = extractLVA(Hits(:,planeCol), Hits(:,stripCol), Hits(:,stationCol));
    %Hack. Our system is incomplete, so layer is always 0. This has to do
    %with the fibers that come to our system. To get good fits, the trigger
    %processor needs to give an offset to the strip # to correct for this
    layer = zeros(size(layer,1),size(layer,2));
	convHits = [bxid, layer, plane, vmmid, art];

	Bxids = unique(bxid);
	for l = 1:numel(Bxids)
        thePacket = constructPackets(convHits, Bxids(l));
        if ~isempty(thePacket)
    		Packets{l,1} = {Bxids(l), thePacket};
        end
    end
    %remove empty packets
    Packets = Packets(~cellfun(@isempty,Packets));
 

	%% constructPackets: given an array of conv hits and a bxid,
	%% produce a cell array of packets to be emitted at this bxid
	%% with the format of each row being: {{layer, side}, {packetData}}
	function [PacketsCell] = constructPackets(hits, bxid)
		hits = hits(find(hits(:,1) == bxid),:);
		layers = unique(hits(:,2));
		pkts = [];
		for k = 1:numel(layers)
			hitsInLayer = hits(find(hits(:,2) == layers(k)),:);
			hitsOnA = hits(find(ismember(hitsInLayer(:,3),planesA)),:);
			hitsOnB = hits(find(ismember(hitsInLayer(:,3),planesB)),:);
			if(~isempty(hitsOnA))
				pkts = [pkts; layers(k), 0, buildPacket(hitsOnA)];
			end
			if(~isempty(hitsOnB))
				pkts = [pkts; layers(k), 1, buildPacket(hitsOnB)];
			end
        end
        if(isempty(pkts))
            PacketsCell = {};
        else
    		PacketsCell = {pkts(:,1:2), pkts(:,3:end)};
        end           
	end

	%% buildPacket: given array of (conv) hits in a given layer and side, arrange them in a packet
	function [Packet] = buildPacket(hits)
		assert(numel(unique(hits(:,1))) == 1, 'All hits must be have the same bxid');
		
		bxid = unique(hits(:,1));

		% fill up eight hits in a proportional way (xxuv 2:1:1)
		xHits = hits(find(ismember(hits(:,3),xPlanes)),:);
		uHits = hits(find(ismember(hits(:,3),uPlanes)),:);
		vHits = hits(find(ismember(hits(:,3),vPlanes)),:);

		xHitsCount = size(xHits,1);
		uHitsCount = size(uHits,1);
		vHitsCount = size(vHits,1);
		totalCount = xHitsCount + uHitsCount + vHitsCount;

		chosenCount = 0;
		chosenHits = [];

		nextX = 1; nextU = 1; nextV = 1;

		function pickHit(planeType)
			switch planeType
				case 'x'
					count = xHitsCount;	pool = xHits; next = nextX;
				case 'u'
					count = uHitsCount;	pool = uHits; next = nextU;
				case 'v'
					count = vHitsCount;	pool = vHits; next = nextV;
			end

			if(next <= count && chosenCount < hitsPerPacket)
				chosenHits = [chosenHits; pool(next, :)];
				next = next + 1;
				chosenCount = chosenCount + 1;
			end

			switch planeType
				case 'x'
					nextX = next;
				case 'u'
					nextU = next;
				case 'v'
					nextV = next;
			end
		end

		while(chosenCount < min(hitsPerPacket, totalCount))
			for r = 1:2 pickHit('x'); end
			pickHit('u');
			pickHit('v');
		end
		
		nOfHits = chosenCount;
		vmmid = chosenHits(:,4);
		art = chosenHits(:,5);
		vmmIDs = [vmmid', repmat(0,1,hitsPerPacket - chosenCount)];
		artData = [art', repmat(0,1,hitsPerPacket - chosenCount)];
		artDataParity = bin2dec(num2str((arrayfun(@(a) computeParity(a), artData))));

		function [parity] = computeParity(x)
			bin = dec2bin(x);
			parity = mod(numel(find(bin == '1')),2);
		end

		Packet = [nOfHits, bxid, vmmIDs, artDataParity, artData];
	end
end