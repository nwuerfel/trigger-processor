function[data] = udpRecDsp()
LocalIPPort = 'LocalIPPort';
port = 6008;
maxPacketLength = 1024;
fileName = 'mmTest';
fileExt = '.txt';
timeout = 5;% in seconds
% make sure to read before the input buffer 8K? fills up
readInterval = .01;
bufSize = 65536

%clear out existing files
for ind = 32:35
    file = [fileName,dec2hex(ind,2),fileExt]
    dlmwrite(file,[],'w');
end

H = dsp.UDPReceiver(LocalIPPort,port,'MaximumMessageLength',maxPacketLength,'ReceiveBufferSize',bufSize)
step(H);%not sure why this first step is needed ??
count = 1;

while count < timeout * 1/readInterval
    udpPacket = dec2hex(step(H));
    udpData = reshape(udpPacket',8,[])';
    if isempty(udpPacket) == 0
        file = [fileName,udpData(2,7:8),fileExt]
        udpPacket
        dlmwrite(file,udpData(3:end,:),'-append','delimiter','');
        type(file)
        count = 1;
    end
    pause(readInterval)
    count = count + 1;
end
disp('UDP Timeout')
release(H)
