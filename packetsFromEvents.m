%% packetsFromEvents: given a vector of event numbers, produce array of (flat) packets
%% corresponding to the hits constituting those events
function [packets] = packetsFromEvents(events,file)
	packets = flattenPackets(assemblePackets(hitsForEvents(events,file)));
end