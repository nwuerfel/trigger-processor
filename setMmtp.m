function[] = setMmtp(binval)
t = tcpOpenMmtp;
regOrig = regRead(t,'00000005')
regBin = dec2bin(hex2dec(regOrig),32);
regBin(31:32) = binval
regReset = dec2hex(bin2dec(regBin),8);
regWrite(t,'00000005',regReset);
regFinal = regRead(t,'00000005')
tcpCloseMmtp(t);