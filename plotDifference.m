function plotDifference(baseCol, difCol, paramName)
	figure
	plot(baseCol, difCol, '*')
	xlabel(sprintf('hard. %s', paramName))
	ylabel(sprintf('hard. %s - sim. %s', paramName, paramName))
	title(sprintf('Difference (computed - simulated) on %s (%d events)', paramName, size(baseCol, 1)));
end