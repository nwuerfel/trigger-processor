%% mat2csvF: given array of Hits and a filename, produce a file with
%% input appropriate for artWrite
function mat2csvF(matArr,outfile)
	outfile

	Hits = matArr; startRow = 1; endRow = size(matArr,1);
	dlmwrite(outfile,Hits(startRow:endRow,[1 5 6 10 24 25 26 27 28 29]))
	type(outfile)
end

