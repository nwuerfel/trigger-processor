function array = JUDPtxt(actionStr,varargin) %#ok<STOUT>
%Code for sending and receiving hex .txt files over UDP.
%to send use   JUDPtxt('send',5001,'128.103.100.174','file.txt')
%
%
%JUDPtxt('send',PORT,ADDR,FILE) sends a file to the specified port and address. 
%
% Adapted from JUDP.m:
% FILE = JUDPtxt('RECEIVE',PORT,PACKETLENGTH, FILE) reconstructs a file from the
% specified port. PACKETLENGTH should be set to the maximum expected
% message length to be received in the UDP packet; if too small, the
% message will be truncated. 
%
% FILE = JUDPtxt('RECEIVE',PORT,PACKETLENGTH, FILE, TIMEOUT) attempts to receive a
% file but times out after TIMEOUT milliseconds. If TIMEOUT is not
% specified, as in the previous example, a default value is used.
%
%Sends text documents to int8 as a UDP packet.
% Developed in Matlab 7.12.0.635 (R2011a) on maci64.
% Hilton Simmet (hsimmet@college.harvard.edu), 20120621 14:48
%   with guidance from Nathan Felt and use of JUDP.m by Kevin Bartlett 
%
%
SEND = 1;
RECEIVE = 2;
DEFAULT_TIMEOUT = 30000;
count = 0;

%Handle input arguments, adapted from K Bartlett, JUDP.m

if strcmpi(actionStr,'send')
    action = SEND;
     port = varargin{1};
     addr = varargin{2};
     file = varargin{3};
else strcmpi(actionStr,'receive')
    action = RECEIVE;
    port = varargin{1};
    packetLength = varargin{2};
    file = varargin{3};
    timeout = DEFAULT_TIMEOUT;
    
    if nargin > 4
        % Override default timeout if specified.
        timeout = varargin{4};
    end % if
end  %if

if action == SEND
%Send .txt file over UDP
fid = fopen(file,'r');                                     %Open the data file
    cela = textscan(fid,'%s %*[^\n]');   %Loads file to cell array
fclose(fid);                                        %Closes the data file.
array = reshape((char(cela{1}))',2,[]); 
array = array';      %Creates array from string array.   
a = dec2bin(hex2dec(array) + 128,9);
pack = bin2dec(a(:,2:9)) - 128; 
intArray = int8(pack);                     %Converts array to int8 dec form.

JUDP(actionStr,port,addr,intArray);
out2 = typecast(intArray,'uint8');
 
else action == RECEIVE    %#ok<EQEFF>
    dlmwrite(file,[],'w');
    %Reconstruct .txt file from UDP
    while 1
        count = count + 1;
    mess = JUDP(actionStr,port,packetLength,timeout);
    C = dec2hex(typecast(mess, 'uint8'));
    array = cellstr((reshape(C',8,[]))');
    str = {['Packet no. ', num2str(count)]};
    dlmcell(file,str,'\n','-a');
    disp(['Packet ',num2str(count),' received succesful.']);
    dlmcell(file,array,'\n','-a');
    return;
    end %while
end %if


