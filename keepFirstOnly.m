function [hwResultsFirst hwResultsExtraHits bcidHWnew] = keepFirstOnly(hwResults,bcidHW)
    hwResultsFirst = [];
    hwResultsExtraHits = [];
    bcidHWnew = [];
    activeBCID = 0;
    for i=1:size(hwResults,1)
        if bcidHW(i)==activeBCID | bcidHW(i)==activeBCID+1 | bcidHW(i)==activeBCID-1
            %check if the first y position of the hit is the lowest
            j = size(hwResultsFirst,1);
            if hwResultsFirst(j,1)<hwResults(i,1)
                hwResultsExtraHits = [hwResultsExtraHits; hwResults(i,:)];
            else
                hwResultsExtraHits = [hwResultsExtraHits; hwResultsFirst(end,:)];
                hwResultsFirst(end,:) = [];
                hwResultsFirst = [hwResultsFirst; hwResults(i,:)];                
            end
        else
            activeBCID = bcidHW(i);
            bcidHWnew = [bcidHWnew; activeBCID];
            hwResultsFirst = [hwResultsFirst; hwResults(i,:)];
        end
    end
    return;
end