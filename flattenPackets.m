%% flattenPackets: given a cell array of Packets, produce a flat array with the same data
%% i.e. [bxid, layer, side, packetData] with only (bxid, layer, side) together being unique
function [flatPacketsArray] = flattenPackets(Packets)
	flatPacketsArray = [];
	for bxK = 1:size(Packets,1)
		bxid = Packets{bxK}{1}
		for layerK = 1:size(Packets{bxK}{2}{1},1)
			assert(size(Packets{bxK}{2}{1},1) == size(Packets{bxK}{2}{2},1));
			layer = Packets{bxK}{2}{1}(layerK,1);
			side = Packets{bxK}{2}{1}(layerK,2);
			packetData = Packets{bxK}{2}{2}(layerK,:);
			flatPacketsArray = [flatPacketsArray; bxid, layer, side, packetData]
		end
	end
end
