%% hexAdd: add two hexadecimal numbers
function[total] = hexAdd(a, b)
	total = dec2hex(hex2dec(a)+hex2dec(b));
end