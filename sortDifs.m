%% sortDifs: given array of Difs as produced by 'analyzeResList', produce
%% a 3-d array where each "page" (matrix along 3rd dimension) contains 
%% an n-by-2 matrix of [event number difference] for a given parameter
%% (and the parameters are in the same order as in Difs, i.e.
%% mx, my, mx_global, mu_global, mv_global, mx_local)

function[sorted] = sortDifs(Difs)
	sorted = zeros(size(Difs,1), 2, size(Difs,2)-1);
	for k = 2:size(Difs,2)
		tmp = sortrows(Difs,k);
		sorted(:,:,k-1) = [tmp(:,1),tmp(:,k)];
	end
end