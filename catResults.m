function[] = catResults()
hold on
percentError = []
eventList = findHitsInRegion(5)
percentError = vertcat(percentError,plotResults(eventList,'../sim/simData/resultsR5.txt'))
eventList = findHitsInRegion(6)
percentError = vertcat(percentError,plotResults(eventList,'../sim/simData/resultsR6.txt'))
eventList = findHitsInRegion(7)
percentError = vertcat(percentError,plotResults(eventList,'../sim/simData/resultsR7.txt'))
eventList = findHitsInRegion(8)
percentError = vertcat(percentError,plotResults(eventList,'../sim/simData/resultsR8.txt'))
percentError(find(abs(percentError(:,6)) > 1),:) = []
figure(6)
hist(percentError(:,5),100)
figure(7)
plot(percentError(:,5),percentError(:,6),'x')
title(['ROI Cartesian coordinate error defined as 100 * (hardware - software)/hardware'])
ylabel(' y ROI Percent error ') % y-axis label
xlabel(' x ROI Percent error ') % y-axis label
figure(8)
hist(percentError(:,5))

hold off
