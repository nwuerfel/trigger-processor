%% unhex: given a hexadecimal number, the exponent by which to shift the value, and whether the value
%% is signed or not, return the decimal representation
function[decimal] = unhex(hex, shift, signed)
	unshifted = hex2dec(hex);
	if(signed)
		unshifted = double(typecast(uint16(unshifted),'int16'));
	end
	decimal = unshifted*2^(shift);
end