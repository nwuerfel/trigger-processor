%% mat2res: given array of Hits, produce array of SimResults with
%% the format: [evNo mx_global mu_global mv_global mx_local mx my]
%% as well as two helper arrays: noResultsFor with event #s of events
%% for which the Hits contain no meaningful simulation results,
%% and noTrackFor with event#s of events for which no track can be formed
%% (the condition on planes is not met).
%% The simulation results are also saved in the file 'SimResults.mat'
function[SimResults, noResultsFor, noTrackFor] = mat2res(matArr)
	
	Hits = matArr;
	startRow = 1; endRow = size(matArr, 1);

	noResultsFor = [];
	noTrackFor = [];

	outFile = 'SimResults.mat'

	nHits = endRow - startRow + 1;

	Hits = sortrows(Hits,1);

	HitsSubset = Hits(startRow:endRow,:);

	nonZeroCol = 24; % the column in HitsSubset which should not be zero
	colsToPick = [1, 24, 25, 26, 27, 28, 29]; % these are the columns from HitsSubset which we want to store as results per event

	firstEvent = HitsSubset(1,1);
	lastEvent = HitsSubset(end,1);

	nHits = numel(HitsSubset(:,1));

	eventsInSubset = unique(HitsSubset(:,1))'
	nEvents = numel(eventsInSubset);
	SimResults = [];

	xPlanes = [1, 2, 5, 6];
	uvPlanes = [3, 4, 7, 8];

	iH = 1;
	for ind = 1:nEvents;
		s_iH = iH;
		while(iH <= nHits && HitsSubset(iH,1) == eventsInSubset(ind))
			iH = iH+1;
		end
		
		eventRows = HitsSubset(s_iH:iH-1,:);
		
		if(numel(eventRows) < 6)
			continue;
		else
			eventPlanes = unique(eventRows(:,5))';
			if(numel(intersect(xPlanes, eventPlanes)) >= 3 && ...
				numel(intersect(uvPlanes, eventPlanes)) >= 3)
				% find non-zero results row and add
				foundRow = false;
				for k = 1:numel(eventRows)
					if(eventRows(k,nonZeroCol) ~= 0)
						SimResults = [SimResults; eventRows(k,colsToPick)];
						foundRow = true;
						break;
					end
				end
				if(~foundRow)
					noResultsFor = [noResultsFor, eventsInSubset(ind)];
					disp(sprintf('No results found for event no. %d', eventsInSubset(ind)))
				end
			else
				noTrackFor = [noTrackFor, eventsInSubset(ind)];
				disp(sprintf('No track possible for event no. %d', eventsInSubset(ind)))
			end
		end
	end

	save(outFile, 'SimResults');


end
