% find events that have all hits within a region

function[eventListAllIn] = findHitsInRegion(region,station,file)
load(file); 
inRegion = zeros(length(Hits),1);
%find index of hits in region
eventIndex = find(Hits(:,5)>=region*512 & Hits(:,5)<(region+1)* 512 & Hits(:,4) == station);
eventList = Hits(eventIndex,1);
%create mask
inRegion(eventIndex) = 1;
eventList = unique(eventList);
%remove events that have members outside of the region
for i = 1:length(eventList)
    eventMembers = find(Hits(:,1) == eventList(i));
    allInRegion(i) = min(inRegion(eventMembers));
end
eventListAllIn = eventList(find(allInRegion == 1));
    
