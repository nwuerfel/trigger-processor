function [eventData] = getEventData(eventNumber,file)
load(file);
%get plane and strip info
eventInfoPS  = Hits(find(Hits(:,1) == eventNumber),:);
[plane index] = sort(eventInfoPS(:,3));
%1 indexing
plane=plane+1;
[bcids index2] = sort(eventInfoPS(:,2));
strip = (eventInfoPS(index,5));
slopes = (eventInfoPS(index,6));
ybasesBottomUse = ybasesBottom;%[ybasesBottom(1,5:8) ybasesBottom(1,1:4)];
stripOffset = round(ybasesBottomUse/0.445); %ybases, station 1
%now handle the case when there is repetition or less planes hit
[plane, indeces, oldplane] = unique(plane);
strip = strip(indeces);
slopes = slopes(indeces);
bcids = bcids(indeces);
strip = strip+stripOffset';
%get info from TP calc
eventInfoAlg  = Event_Fit_Info(eventNumber,:);
eventData = zeros(1,8);
slopeData = zeros(1,8);
eventData(plane) = strip';
slopeData(plane) = slopes';
goodAlgInfo = eventInfoAlg([5:8]);
bcids = mod(bcids,power(2,12));
eventData = [eventData goodAlgInfo bcids' slopeData eventNumber];


