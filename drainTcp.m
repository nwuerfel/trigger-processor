function[] =  drainTcp(tcpObj)
timeout = 20;
timeWait= 0;

while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
    pause(.1)
    timeWait = timeWait+1;
end

while(get(tcpObj, 'BytesAvailable') > 0)
    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4,'uint32'))
end