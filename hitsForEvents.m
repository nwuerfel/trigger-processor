%% hitsForEvents: given a vector of event numbers, produce array of Hits 
%% corresponding to those events 
function[hits] = hitsForEvents(eventVector,file)

	%% error checking on event vector
	if(~isvector(eventVector))
		disp('Bad input - must be a vector');
		return;
	end
	if(iscolumn(eventVector))
		eventVector = eventVector';
	end

	%% load up the file containing data
	load(file);

	%% create a fresh array to store relevant data from file
	hits = [];

	%% populate the array with data from the file
	for event = 1:numel(eventVector)
		hits = [hits; Hits(find(Hits(:,1)==eventVector(event)),:)];
	end
	disp(sprintf('Found %d hits.', size(hits,1)));
end
