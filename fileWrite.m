function[tcpPayload] =  fileWrite(tcpObj,addr,inFile)
header = 'abcd1234'
type = 'FE170002'
fp = fopen(inFile, 'rt');
hex2dec([header;type;addr])
tcpPayload = [hex2dec([header;type;addr]);fscanf(fp, '%x')];
fwrite(tcpObj,tcpPayload, 'uint32');
timeout = 20;
timeWait= 0;
fclose(fp)

while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
    pause(.1)
    timeWait = timeWait+1;
end

while(get(tcpObj, 'BytesAvailable') > 0)
    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4, ...
                                 'uint32'))
end
