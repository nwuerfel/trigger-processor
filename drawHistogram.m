function drawHistogram(difCol, paramName)
	figure
	hist(difCol)
	xlabel(sprintf('hard. %s - sim. %s', paramName, paramName))
	ylabel('frequency')
	title(sprintf('Histogram of the difference (computed - simulated) on %s (%d events)', paramName, size(difCol,1)))
end
