%calculate MA and MB
zbases = [7583.5    7594.5   7752.5    7763.5];
selections = [1 1 1 1; 1 1 1 0; 1 1 0 1; 1 1 1 0; 1 0 1 1; 1 0 1 0;...
    1 0 0 1; 0 1 1 1; 0 1 1 0; 0 1 0 1; 0 1 1 1];
A = [];
B = [];
zinv = [];
for i=1:size(selections,1)
    zbasesSel = zbases;
    for j=1:size(zbasesSel,2)
        zbasesSel(j) = zbasesSel(j)*selections(i,j);
    end
    Atemp = sum(selections(i,:))/(sum(selections(i,:))*zbasesSel*zbasesSel'-...
        sum(zbasesSel)*sum(zbasesSel));
    A = [A; Atemp];
    Btemp = sum(zbasesSel)*Atemp/sum(selections(i,:));
    B = [B; Btemp];
    zinv = [zinv; sum(zbasesSel)/sum(selections(i,:))];
end
stripPitch = 0.445;
zinv = power(zinv,-1)*stripPitch;
A = stripPitch*A;
B = stripPitch*B;
zinv = zinv;
zbases = zbases;
zzinvSoft = (zbases'*zinv')';
zzinvSoft = zzinvSoft-ones(size(zzinvSoft));
zinvfi = ufi(zinv,32);
zbasesfi = sfi(zbases);
zzinvHw = (zbasesfi'*zinvfi')';
sizezzinvHW = size(zzinvHw); 
zzinvHwBin24 = repmat(sfi(0,16,13),sizezzinvHW(1),sizezzinvHW(2));
for i=1:sizezzinvHW(1)
    for j=1:sizezzinvHW(2)
        element = zzinvHw(i,j);
        fielement = sfi(zzinvHwBin24,16,13);
        fielement.bin = element.bin(1:16);
        zzinvHwBin24(i,j) = fielement-1;
    end
end

(zzinvHwBin24.data-zzinvSoft)./zzinvSoft