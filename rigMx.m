%% rigMx: given a reslist, "rig" the values of mx to correct for the sign error;
%% that is, set the most significant bit to '1' by hand whenever this improves
%% the agreement between the hardware output and simulation
function[riggedResList, rigs] = rigMx(resList)
	mx_har_i = 27;
	mx_sim_i = 38;
	% rig_threshold = 0.01;

	riggedResList = resList;
	rigCount = 0;
	rigs = [];

	for k = 1:size(resList,1)
		mx_h = resList(k, mx_har_i);
		mx_s = resList(k, mx_sim_i);
		mx_dif = abs(mx_h) - mx_s;
		rig_mx = unhex(hexAdd(rehex(mx_h,17),'8000'),-17,1);
		rig_dif = abs(rig_mx) - mx_s;

		if(abs(rig_dif) < abs(mx_dif))
			riggedResList(k, mx_har_i) = rig_mx;
			rigCount = rigCount + 1;
			rigs = [rigs, resList(k,1)];
		end
	end

	rigCount
end
