%% pack2sim: given array of packet data ([bxid layer side packetData]), produce simulation input
%% optional second argument: outfile - full filename of file to produce
function pack2sim(packets, outfile)
	if(~exist('outfile', 'var'))
		outfile = 'packetDataIn_v1.txt'; 
	end
	pgoutfile0 = ['pg0',outfile];
	pgoutfile1 = ['pg1',outfile];
        
    [addcFormat pgFormat0 pgFormat1] = packetToBinary(packets(:,4:end),packets(:,2));
	addcPacketHex = binStr2hexStr(addcFormat);
        
        %%! pattern generator file formatter assumes 1 PG!!
        %the vmm id  is mod 32 in this case
        
	pgPacketHex0 = binStr2hexStr(pgFormat0);
	pgPacketHex1 = binStr2hexStr(pgFormat1);
    % the flip was added to number the chunks with the LSB in
    % the lowest number chunk
    addcPacketHex = [repmat('0000', size(addcPacketHex,1),1),addcPacketHex];
	chunks =  reshape(addcPacketHex',8,[])';

    %changed to include the side + layer in fifo address
    % this will change from one fifo/layer to one fifo/ADDC fiber.
    packetFiber = packets(:,2)*2  + packets(:,3) + 32;
    layerInfo = dec2hex(reshape(repmat((packetFiber),[1 4])',1,[])',2);
	simData = [chunks,repmat(' ',size(chunks,1),1),layerInfo];
        
    %all hits go to 1 set of four MMFEs (1 PG dev board)
    %need to write multiple files to support more than one PG
    %dev board
	pgSimData0 = [pgPacketHex0(:,1:8),repmat(' ', ...
                                                     size(pgPacketHex0,1),1),pgPacketHex0(:,9:10)];
	pgSimData1 = [pgPacketHex1(:,1:8),repmat(' ', ...
                                                     size(pgPacketHex1,1),1),pgPacketHex1(:,9:10)];

	dlmwrite(outfile, simData, ''); 
	dlmwrite(pgoutfile0, pgSimData0, ''); 
	dlmwrite(pgoutfile1, pgSimData1, ''); 

	disp(sprintf('Content of %s:',outfile));
	type(outfile)
        
	disp(sprintf('Content of %s:',['pg0',pgoutfile0]));
	type(pgoutfile0)
	disp(sprintf('Content of %s:',['pg1',pgoutfile1]));
	type(pgoutfile1)
end