
% y = [3192; 3198; 3211; 3213; 3246; 3251; 3265; 3267]
%y = [5101;5111;5159;5106;5188;5195;5247;5193]
%y = [6229;6239;6267;6268;6332;6341;6370;6370]
%y = [10;20;30;40;50;60;70;80]

inFile = '426-fifoOut.txt';
fifoData = importdata(inFile);
fifoData
% f = [double(hex2dec(fifoData(1,1:4)))*2^0,... %TRACK_DATA.Y(1) 1
%     double(hex2dec(fifoData(1,5:8)))*2^0,... %TRACK_DATA.Y(0) 2
%     double(hex2dec(fifoData(2,1:4)))*2^0,... %TRACK_DATA.Y(3) 3
%     double(hex2dec(fifoData(2,5:8)))*2^0,... %TRACK_DATA.Y(2) 4
%     double(hex2dec(fifoData(3,1:4)))*2^0,... %TRACK_DATA.Y(5) 5
%     double(hex2dec(fifoData(3,5:8)))*2^0,... %TRACK_DATA.Y(4) 6
%     double(hex2dec(fifoData(4,1:4)))*2^0,... %TRACK_DATA.Y(7) 7
%     double(hex2dec(fifoData(4,5:8)))*2^0,... %TRACK_DATA.Y(6) 8
%     ];
f = formatTPDebug(cell2mat(fifoData));
y = [f(:,2); f(:,1); f(:,4); f(:,3); f(:,6); f(:,5); f(:,8); f(:,7)];

% y= [5325; 5333; 5351; 5365; 5414; 5422; 5442; 5454];

z = ['1D36';'1D41';'1D61';'1D6C';'1DB4';'1DBF';'1DDF';'1DEA']
zr =['F998';'F93A';'F82A';'F7CE';'F575';'F51A';'F413';'F3BA']
A = hex2dec('00E95A17')
B = hex2dec('00D6F6F1')


mx = dec2hex(floor(y .* hex2dec(zr) * 2^-16),4)
mx(:,[1 2])

% calc mx local
zx = hex2dec(z([1 2 5 6],:))
yx = y([1 2 5 6],:)
szy = floor(sum(zx .* yx)*2^-5)
dec2hex(szy)
aszy = floor((szy * A)*2^-20)
dec2hex(aszy)
sy = floor(sum(yx))
dec2hex(sy)
bsy = floor(sy*2^-12 * B)  
dec2hex(bsy)
mxl = (aszy - bsy)* 2^-14
dec2hex(aszy - bsy)


trackm(1,:) = reshape(mx([2 1],:)',1,[])
trackm(2,:) = reshape(mx([6 5],:)',1,[])
trackm(3,:) = reshape(mx([7 3],:)',1,[])
trackm(4,:) = reshape(mx([8 4],:)',1,[])
mxg = dec2hex(floor(mean(hex2dec(mx([6 5 2 1],:)))))
mug = dec2hex(floor(mean(hex2dec(mx([7 3],:)))))
mvg = dec2hex(floor(mean(hex2dec(mx([8 4],:)))))

mxg_conv = hex2dec(mxg) *2^-14
mug_conv = hex2dec(mug) *2^-14
mvg_conv = hex2dec(mvg) *2^-14

%calc ROI cart cord
sa = 1.5
csc_sa = csc(2*pi * sa/360) %hex2dec('98CE')
cot_sa = cot(2*pi * sa/360) %hex2dec('98C0')
% csc_sa = hex2dec('98CE')
% cot_sa = hex2dec('98C0')
dec2hex(floor(csc_sa * hex2dec(mug)))
dec2hex(floor(cot_sa * hex2dec(mxg)))
dec2hex(floor(cot_sa * hex2dec(mxg)))
dec2hex(floor(csc_sa * hex2dec(mvg)))
ROI_mx(1) = csc_sa * hex2dec(mug) - cot_sa * hex2dec(mxg)
ROI_mx(2) = cot_sa * hex2dec(mxg) - csc_sa * hex2dec(mvg)

ROI_mx_cart = mean(ROI_mx)* 2^-14

dec2hex(mean(ROI_mx))

