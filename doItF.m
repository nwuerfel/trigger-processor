%% doItF: given a file of input hit data, run the hit data from the file through the hardware
%% and return the fifo output; also, save the fifo output to '<input_file_name>-Out.csv'
function [fifoData] = doItF(inFile)

	t = tcpOpenMmtp          
	inData = dlmread(inFile);
	maxRowsPerWrite = 100; % this is to limit the size of the tcp packets sent
	finalRow = size(inData,1);
	lastRow = 1;
	disp(sprintf('Total rows in file: %d', finalRow));
	while(lastRow < finalRow)
		prevRow = lastRow;
		lastRow = min(finalRow, prevRow+maxRowsPerWrite);
		disp(sprintf('Writing rows %d-%d',[prevRow, lastRow]));
		artWriteRows(t, inData(prevRow:lastRow, :));
	end
	status(t)

	startDaq(t)                            
	fifoData = drainFifo(t,'00000020');
	dlmwrite(strcat(inFile,'-Out.csv'), fifoData, '');

	status(t)
	tcpCloseMmtp(t)
end
