function makeArtTestPattern_bak(outfile)
if(~exist('outfile', 'var'))
    outfile = 'artTestData'; 
end 
fileData = [];
answer = [];
for hitNumber = 1:15
    % assume one hit per mmfe8
    mmfe = dec2bin(randperm(4)-1,2)  ;
    %mmfe = dec2bin([0;1;2;3],2)  ;
    %find 4 strip numbers from 0 to 511
    mmfeStrip = dec2bin((randi(512,[4,1])-1),9);
    hit = [mmfe,mmfeStrip];
     answer  = vertcat(answer,dec2hex(bin2dec(mmfeStrip),3));
    %there are 32 vmms per addc
    vmmid = hit(:,1:5);
    vmmStrip = hit(:,6:11);
  plane = dec2hex(bin2dec(hit(:,1:2)))
  chipStrip = dec2hex(bin2dec(hit(:,6:11)))
    % pktHitCnt = dec2hex(hitNumber,1);
    pktHitCnt = '4';
   pktBcid = dec2hex(hitNumber*16 + 16,3);
    pktVmmId = dec2hex(bin2dec(reshape(vmmid',1,[])),10) ;
    pktParity = num2str((arrayfun(@(a) computeParity(a), bin2dec(vmmStrip))));
    pktParity = dec2hex(bin2dec(pktParity'),2);
    
     pktArt = dec2hex(bin2dec(reshape(vmmStrip',1,[])),12)   ;
   
     artTestData = ['0000',pktHitCnt,pktBcid,pktVmmId,pktParity,pktArt];
     artTestData = reshape(artTestData,8,[])';
     artTestData =  [artTestData, repmat(' 20',size(artTestData,1),1)];

     fileData  = vertcat(fileData,artTestData);
  
end
answer = [repmat('0',size(answer,1),1),answer]

%trigger processor orders planes in decending order
%while keeping the event order
answer = flipud(answer)
answer = reshape(answer',16,[])'
answer = flipud(answer)

dlmwrite([outfile,'Ans.txt'],answer,'delimiter','');

     dlmwrite([outfile,'.txt'],fileData,'delimiter','');
     fileData  = vertcat(fileData,'00000001 01');
     dlmwrite([outfile,'WStart.txt'],fileData,'delimiter','');
disp(fileData)
disp(answer)
end
    function [parity] = computeParity(x)
        bin = dec2bin(x);
        parity = mod(numel(find(bin == '1')),2);
    end

