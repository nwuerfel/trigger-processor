function[DataReceived] =  bxidReset(tcpObj)
header = 'abcd1234';
type = 'FE170002';
addr = '00000001'; %command address;

%clear ART FIFOs
data = '00000002'; %reset on;
tcpPayload = hex2dec([header;type;addr;data]);
fwrite(tcpObj,tcpPayload, 'uint32');
pause(.1)
data = '00000003'; %reset off;
tcpPayload = hex2dec([header;type;addr;data]);
fwrite(tcpObj,tcpPayload, 'uint32');
pause(.1)
 data = '00000004'; %reset BXID;
tcpPayload = hex2dec([header;type;addr;data]);
fwrite(tcpObj,tcpPayload, 'uint32');
pause(.1)

