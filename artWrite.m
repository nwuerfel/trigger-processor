%% artWrite: given a tcp object and a hit data input file, sent the hits to
%% the hardware in a tcp packet, and return any data received in response
function[DataReceived] =  artWrite(tcpObj,inFile)
	header = 'abcd1234';
	type = 'FE170002';
	addr = '00000013';
	hex2dec([header;type;addr]);
	dataIn = dlmread(inFile);
	dataIn = dataIn(:,2:4);
	dataIn = reshape(dataIn,[],3);
	dataInDC =  ((dataIn(:,1)-1)*2^16) + (dataIn(:,2)*2^0) + (mod(dataIn(:,3),4096)*2^20);
	tcpPayload = [hex2dec([header;type;addr]);dataInDC]  ;
	fwrite(tcpObj,tcpPayload, 'uint32');
	timeout = 20;
	timeWait= 0;

	while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
	    pause(.1)
	    timeWait = timeWait+1;
	end

	while(get(tcpObj, 'BytesAvailable') > 0)
	    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4, ...
	                                 'uint32'));
	end
end