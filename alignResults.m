%% alignResults: given the original Hits sent to the hardware, the hardware output
%% and the simulation results, attempt to align the harware and simulation results
%% and return an array with rows of the form [event number, hardware results, simulation results]
%% (a 'reslist')
function [resList] = alignResults(HitsSent, hardwareResults, simulatedResults)
	L = log4m.getLogger('log');

	eventsSent = unique(HitsSent(:,1));
	inputBcs = arrayfun(@(event) getBcForEvent(event, HitsSent), eventsSent(:,1));

	% parameters
	hwBcCol = 31;
	delay = 56;
	FPGAres = 15;
	FPGAtoBcClock = 3;
	rolloverThreshold = 4096;

	function outputBc = getOutputBc(inputBc)
		outputBc = mod(mod(inputBc, rolloverThreshold)*2^FPGAtoBcClock + delay, 2^FPGAres);
	end

	% initialize
	inputRollover = 0;
	outputRollover = 0;
	noToAlign = size(eventsSent,1);
	prevEvent = eventsSent(1);
	prevInputBc = inputBcs(1);
	prevOutBc = hardwareResults(1,hwBcCol);
	hwRows = zeros(noToAlign, 1);
	smRows = zeros(noToAlign, 1);

	hrInd = 1;
	function [ind, prevBc] = stepHrInd(thisInd, thisBc)
		ind = thisInd + 1;
		prevBc = thisBc;
	end

	L.info('ALIGN', sprintf('--Attempting to align results for %d tracks (%d harware results, %d software results)',...
		[noToAlign, size(hardwareResults, 1), size(simulatedResults,1)]));

	% do all the aligning
	for k = 1:noToAlign
		thisEvent = eventsSent(k); thisInputBc = inputBcs(k);
		
		% match simulation row
		thisSmRow = find(simulatedResults(:,1) == thisEvent);
		if(isempty(thisSmRow))
			thisSmRow = -1;
		end
		smRows(k) = thisSmRow;

		% match hardware row
		% are we crossing a rollover point?
		if(fix(thisInputBc/rolloverThreshold) > fix(prevInputBc/rolloverThreshold))
			inputRollover = inputRollover + 1;
		end
		% now look for matching bc
		bcToFind = getOutputBc(thisInputBc);
		matched = false;
		while(~matched)
			% find a match
			% or conclude that match is unavailable
			
			if(hrInd > size(hardwareResults,1)) % ran out of hardware results!
				matched = true;
				thisHwRow = -1;
			else
				thisOutBc = hardwareResults(hrInd, hwBcCol);

				if(thisOutBc < prevOutBc)
					outputRollover = outputRollover + 1;
				end
				
				if(inputRollover > outputRollover) % there HAS to be a rollover so force it to catch up
					outputRollover = inputRollover;
					% [hrInd, prevOutBc] = stepHrInd(hrInd, thisOutBc);
				end

				if(inputRollover == outputRollover)
					% disp(sprintf('to find: %d, found: %d',[bcToFind, thisOutBc]));
					if(bcToFind < thisOutBc) % missing hardware result
						matched = true;
						thisHwRow = -1; 
					end
					if(bcToFind == thisOutBc) % correct match found
						matched = true;
						thisHwRow = hrInd;
						[hrInd, prevOutBc] = stepHrInd(hrInd, thisOutBc);
					end
					if(bcToFind > thisOutBc) % apparently too many outputs at this bc
						[hrInd, prevOutBc] = stepHrInd(hrInd, thisOutBc);
					end
				end
				
				if(inputRollover < outputRollover) % there was no match at the right rollover
					matched = true;
					thisHwRow = -1;
				end
			end
		end
		hwRows(k) = thisHwRow;
	end

	alignments = [eventsSent, hwRows, smRows];

	% put together reslist from correct alignments

	availableAlignments = find(alignments(:,2) > 0 & alignments(:,3) > 0);

	resList = [eventsSent(availableAlignments),...
		       hardwareResults(alignments(availableAlignments, 2), :),...
		       simulatedResults(alignments(availableAlignments, 3), :)];

	alMissHr = eventsSent(find(alignments(:,2) == -1));
	alMissSr = eventsSent(find(alignments(:,3) == -1));
	alOk = eventsSent(availableAlignments);
	noResponse = setdiff(alMissHr,alMissSr);
	save('alStats.mat', 'alignments', 'alMissHr', 'alMissSr', 'alOk', 'noResponse');

	if(~isempty(alMissHr))
		L.warn('ALIGN', sprintf('--Alignment problem: missing %d hardware results, including %d with simulation results present.', [numel(alMissHr), numel(alMissHr) - numel(intersect(alMissSr, alMissHr))]));
		% disp(sprintf('--Alignment problem: missing %d hardware results, including %d with simulation results present.', [numel(alMissHr), numel(alMissHr) - numel(intersect(alMissSr, alMissHr))]));
		% alMissHr
	end
	if(~isempty(alMissSr))
		L.warn('ALIGN', sprintf('--Alignment problem: missing %d simulated results, including %d with hardware results present.', [numel(alMissSr), numel(alMissSr) - numel(intersect(alMissSr, alMissHr))]));
		% disp(sprintf('--Alignment problem: missing %d simulated results, including %d with hardware results present.', [numel(alMissSr), numel(alMissSr) - numel(intersect(alMissSr, alMissHr))]));
		% alMissSr
	end
	L.info('ALIGN', sprintf('--Successfully aligned %d results.', numel(alOk)));
	% disp(sprintf('--Successfully aligned %d results.', numel(alOk)));
end
