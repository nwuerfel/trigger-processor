function[] = resetGbtTx()
t = tcpOpenMmtp;
regOrig = regRead(t,'00000004')  ;
regBin = dec2bin(hex2dec(regOrig),32)
regBin(end-10) = '1';
regReset = dec2hex(bin2dec(regBin),8);
regWrite(t,'00000004',regReset);
regWrite(t,'00000004','0000020C');
tcpCloseMmtp(t);