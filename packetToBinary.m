%% packetToBinary: convert packet array to binary form
function [addcFormat, pgFormat0, pgFormat1] = packetToBinary(packet,side)
% packet = [nOfHits, bxid, vmmIDs(x8), artDataParity, artData(x8)]
    disp('starting packetToBinary')
    packet= packet;
    side= side;
    nOfHits = packet(:,1);
    bxid =  packet(:,2);
    vmmIDs = mod(packet(:,3:10),2^5);
    pgSide = floor(packet(:,3:10) ./ 2^5);
    artData = packet(:,12:19);
    
    hitMap = reshape(dec2bin(zeros(size(packet,1),32)), ...
                     size(packet,1),[]);
    
    pgBxid = dec2bin(mod(bxid, 2^26), 26);
    tpBxid = dec2bin(mod(bxid, 2^12), 12);
    pgFormat1 = [];
    pgFormat0 = [];
    for packInd = 1:size(packet,1)
        disp (['event:',dec2hex(bin2dec(pgBxid(packInd,:)), 3)])
        vmmRange = 1:nOfHits(packInd);
        %pattern generator conversion one line per hit
        for hitInd = vmmRange
            pgArtData = dec2bin(mod(artData(packInd,hitInd)', 2^6), ...
                                6); 
            pgVmmData = dec2bin(mod(vmmIDs(packInd,hitInd)', 2^5), ...
                                5);
            % disp vmm /art data 
            disp(['VMM ', dec2hex(mod(vmmIDs(packInd,hitInd)', 2^5), 2)...
                  ,' ART ', dec2hex(mod(artData(packInd,hitInd)', 2^6), 2)])
            
            pgEntry = [pgBxid(packInd,:),pgArtData, ...
                       '000',pgVmmData];
            if pgSide(packInd,hitInd) == 1
                pgFormat1 = vertcat(pgFormat1, pgEntry);
            else
                pgFormat0 = vertcat(pgFormat0, pgEntry);
                
            end
        end
        % addc format conversion
        disp('here')
        vmmIDs(packInd,:)
        %set the unhit vmms to the max + 1 to proberly sort
        vmmIDs(packInd,nOfHits(packInd)+1:8) = 33;
        [~, vmmOrder] = sort(vmmIDs(packInd,:),2);
        artData(packInd,:) = fliplr(artData(packInd,vmmOrder));
        hitMap(packInd,vmmIDs(packInd,vmmRange)+1) = '1';
        
    end           
    hitMap = fliplr(hitMap);
    
    %     debug = dec2bin(debug,5)'
    % debug = reshape(debug,[],size(packet,1))'
    artDataParity = dec2bin(mod(packet(:,11), 2^8), 8);
    artData = reshape(dec2bin(mod(artData', 2^6), 6)',[],size(packet,1))';
    
    %% line left for easier debugging: sections split by '-' separator
    % bin = strcat(nOfHits,'-',bxid,'-',vmmIDs,'-',artDataParity,'-',artData);
    
    addcFormat = strcat('1010',tpBxid, '00000000', hitMap, artDataParity, ...
                        artData);
end
