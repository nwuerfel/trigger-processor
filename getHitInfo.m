function [eventData] = getHitInfo(eventNumber)
load('nathan.mat');
%get plane and strip info
eventInfoPS  = Hits(find(Hits(:,1) == eventNumber),:);
[plane index] = sort(eventInfoPS(:,5));
strip = (eventInfoPS(index,6));
%get info from TP calc
find(Hits(:,1) == eventNumber);
eventInfoAlg  = Hits(find(Hits(:,1) == eventNumber),:);
eventData = zeros(1,8);
eventData(plane) = strip';
goodAlgInfo = eventInfoAlg([24:29,13]);
eventData = [eventData goodAlgInfo];
