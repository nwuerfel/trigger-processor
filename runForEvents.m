%% runForEvents: given a vector of event numbers, run the data for those events through the
%% entire process and return
%% responseCount    - the number of lines in the fifo output
%% hardwareResults  - the total array of results from the hardware in decimal representation
%% 					(as given by formatTPDebug)
%% simulatedResults - the simulation results for the events specified
%% reslist          - hardware and simulation results aligned in one array
%% hits             - the hits chosen from Brian's array for the run
function [responseCount, hardwareResults, simulatedResults, reslist, hits] = runForEvents(eventsVector)
	tic
	load('nathan.mat');

	HitsToProcess = hitsForEvents(eventsVector);

	maxEventsPerPacket = 110;
	maxRowsPerPacket = 8 * maxEventsPerPacket; % this is to prevent input fifo from overfilling
	
	evCol = HitsToProcess(:,1);
	
	firstEvent = evCol(1,1);
	lastEvent = evCol(end,1);

	mat2sim(HitsToProcess);
	[HR, noResFor, noTraFor] = mat2res(HitsToProcess);

	events = unique(evCol)';

	firstInd = 1; lastInd = numel(events);
	nPackets = 0;
	packetBounds = [];
	fifoOutput = ['F0000000']; % add header as expected by formatTPDebug
	ind = firstInd;
	while(ind <= lastInd)
		fIndInPacket = ind; % debug
		eventsInPacket = 0; % debug
		startRow = firstRowForEvent(evCol, events(ind));
		tInd = ind;
		while(tInd <= lastInd)
			tEndRow = lastRowForEvent(evCol, events(tInd));
			if(tEndRow - startRow + 1 <= maxRowsPerPacket)
				endRow = tEndRow;
				tInd = tInd + 1;
				eventsInPacket = eventsInPacket + 1;
			else
				break;
			end
		end
		ind = tInd;
                		fifoOutput = [fifoOutput; doItForRows(HitsToProcess, startRow, endRow)];
                                %%%%%		fifoOutput = [fifoOutput; doItForRowsHWSim(HitsToProcess, startRow, endRow)];
		packetBounds = [packetBounds; startRow, endRow,...
						events(fIndInPacket), events(ind-1),...
						endRow-startRow+1, eventsInPacket]; % debug
		nPackets = nPackets + 1
	end
	responseCount = numel(fifoOutput(:,1))
	hardwareResults = [];
	simulatedResults = [];

	if(responseCount > 1)
		hardwareResults = formatTPDebug(fifoOutput)

		simulatedResults = HR
		
		disp(sprintf('--Sim. results not avail. for %d event(s).', numel(noResFor)));
		noResFor
		noTraFor

		reslist = alignResults(HitsToProcess, hardwareResults, simulatedResults);
	else
		disp('--No data in reponse!');
		reslist = [];
	end
	hits = HitsToProcess;
	toc
end

function[row] = lastRowForEvent(eventsColumn, eventNo)
	bound = find(eventsColumn > eventNo, 1, 'first');
	if(isempty(bound))
		row = numel(eventsColumn);
	else
		row = max(1, bound-1);
	end

	% disp(sprintf('Finding LAST  row for ev no. %d: %d',[eventNo,row]));
end

function[row] = firstRowForEvent(eventsColumn, eventNo)
	bound = find(eventsColumn < eventNo, 1, 'last');
	if(isempty(bound))
		row = 1;
	else
		row = min(bound+1, numel(eventsColumn));
	end

	% disp(sprintf('Finding FIRST row for ev no. %d: %d',[eventNo,row]));
end

function[output] = doItForRows(matArr, startRow, endRow)
	packets = flattenPackets(assemblePackets(matArr(startRow:endRow,:)));
	pack2sim(packets);

	output = doPackets('packetDataIn.txt');
end
% add option to get results from Matlab simulation
function[output] = doItForRowsHWSim(matArr, startRow, endRow)
fileID = fopen('../sim/simData/results.txt');
output = textscan(fileID,'%s')
output = cell2mat(output{1})

fclose(fileID);
end