function array = mmtpUdpRecieve()

% this function is based on JUDPtxt and JUDPplot_s

SEND = 1;
RECEIVE = 2;
DEFAULT_TIMEOUT = 2000;
count = 0;
    port = 6008;
    packetLength = 1024;
    fileName = 'mmTest';
    fileExt = '.txt'
    timeout = 20000;
    hitSum = zeros(64,1)';
    for ind = 32:35
        file = [fileName,dec2hex(ind,2),fileExt]
        dlmwrite(file,[],'w');
    end
    
    %Reconstruct .txt file from UDP
    while 1
        count = count + 1;
        mess = JUDP('receive',port,packetLength,timeout);
        C = dec2hex(typecast(mess, 'uint8'));
        
        array = cellstr((reshape(C',8,[]))')
        addr = cell2mat(array(2))
        file = [fileName,addr(7:8),fileExt]
        dlmwrite(file,array(3:end,:),'-append','delimiter','');
        
        if length(array) > 3
            arrayBin = dec2bin(hex2dec(cell2mat(array)),32)
        end
        %return;
    end %if


