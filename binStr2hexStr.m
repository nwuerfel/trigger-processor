%% binStr2hexStr: convert array of binary strings to array of hex strings,
%% adding 0s to the front as needed
function [hexStr] = binStr2hexStr(binStr)
	if(mod(size(binStr, 2), 4) ~= 0)
		binStr = strcat(repmat('0', 1, 4-mod(size(binStr, 2), 4)));
	end
	hexLength = size(binStr, 2)/4
	hexStr = reshape(reshape(dec2hex(bin2dec(reshape(binStr',4,[])'),1),1,[])', hexLength, [])'
end
