% generate GBT packets that contin random hits
%output 2 files with 4 32-bit words/packet
%msim (WStart) file has the command to start DAQ at end
%the Answer file will be used with the 'checkArtTestPattern' to
%check for errors
%currently this file only contains the mmfe strip segmet of the
%Debug3 results.
%assumes first 1/16 of wedge

function makeArtTestPattern(outfile)
if(~exist('outfile', 'var'))
    outfile = 'artTestData'; 
end
 Z = hex2dec(['F998'; 'F93A'; 'F82A'; 'F7CE'; 'F575'; 'F51A'; 'F413'; 'F3BA'])
 
fileData = [];
hit_y = [];
hit_m = [];
for hitNumber = 5:30
    if mod(hitNumber,8) == 0
    % assume one hit per mmfe8
    mmfe = dec2bin(randperm(4)-1,2)  ;
    %mmfe = dec2bin([3;2;1;0],2)  ;
    %find 4 strip numbers from 0 to 511
    mmfeStrip = dec2bin((randi(512,[4,1])-1),9);
    hit = [mmfe,mmfeStrip];
  %there are 32 vmms per addc
    vmmid = hit(:,1:5);
    vmmStrip = hit(:,6:11);
    plane = bin2dec(hit(:,1:2))
    chipStrip = dec2hex(bin2dec(hit(:,6:11)))
    bin2dec(mmfeStrip)
    %the trigp orders data decending in debug fifo
    [b,sortIndex] = sort(plane,'descend')
     mmfeStripSort = bin2dec(mmfeStrip(sortIndex,:))
     mmfeStripSortSlope = floor((mmfeStripSort .* Z(4:-1:1))/2^16)
     hit_y  = vertcat(hit_y,dec2hex(mmfeStripSort,4));
     hit_m  = vertcat(hit_m,dec2hex(mmfeStripSortSlope,4));
      % pktHitCnt = dec2hex(hitNumber,1);
    %fixed to 4 hits
    pktHitCnt = '4';
    pktBcid = dec2hex(hitNumber,3);
    pktVmmId = dec2hex(bin2dec(reshape(vmmid',1,[])),10) ;
    pktParity = num2str((arrayfun(@(a) computeParity(a), bin2dec(vmmStrip))));
    pktParity = dec2hex(bin2dec(pktParity'),2);
    
    pktArt = dec2hex(bin2dec(reshape(vmmStrip',1,[])),12)   ;
    
    artTestData = ['0000',pktHitCnt,pktBcid,pktVmmId,pktParity, ...
                   pktArt];
    artTestData = reshape(artTestData,8,[])';
    artTestData =  [artTestData, repmat(' 20',size(artTestData,1),1)];
    
    fileData  = vertcat(fileData,artTestData);
    else
        
    pktHitCnt = '0';
    pktBcid = dec2hex(hitNumber,3);
    pktVmmId = '0000000000' ;
    pktParity = '00'; 
    pktArt = '000000000000';
    
    artTestData = ['0000',pktHitCnt,pktBcid,pktVmmId,pktParity, ...
                   pktArt];
    artTestData = reshape(artTestData,8,[])';
    artTestData =  [artTestData, repmat(' 20',size(artTestData,1),1)];
    
    fileData  = vertcat(fileData,artTestData);
        
    end
    
 
end
     disp('bingo')

    binTestData = dec2bin(hex2dec(fileData(:,1:8)),32)
    binTestData =  reshape (binTestData',128,[])'
hit_y = reshape(hit_y',16,[])'
hit_m = reshape(hit_m',16,[])'
%hit_m = reshape(hit_m',16,[])'
dlmwrite([outfile,'Ans.txt'],[hit_y,hit_m],'delimiter','');

dlmwrite([outfile,'.txt'],fileData,'delimiter','');


dlmwrite([outfile,'.bit'],binTestData,'delimiter','');

fileData  = vertcat(fileData,'00000001 01');
dlmwrite([outfile,'WStart.txt'],fileData,'delimiter','');
disp(fileData)
disp(hit_y)
end
function [parity] = computeParity(x)
    bin = dec2bin(x);
    parity = mod(numel(find(bin == '1')),2);
end

