%function[swpl hwpl] = plotResults(eventList,file)
function[percentError] = plotResults(eventList,file,originalHits)
load(originalHits);
printLimit = 1

hwResultsAll = formatTPDebug(file)

swResults = [];
for eventIndex = 1:length(eventList)
    swResults  = vertcat(swResults,getEventData(eventList(eventIndex),originalHits));
end

%software results data format
%strip plane 0:7
%dtheta
%mx global
%mu global
%mv global
%mx local
%mx
%my

% pick out data from HW results to match order in SW results
bcidSW = swResults(:,13:20)
hwResults = hwResultsAll(:,[9 8 5 3 4 2 7 6 24 23 22 21 20 24 ...
    17 16 13 11 12 10 15 14]);

bcidHW = hwResultsAll(:,1)-4;
[hwResultsFirst hwResultsExtraTracks bcidHWnew] = keepFirstOnly(hwResults,bcidHW);
[swResultsPass swResultsFail] = keepCommonOnly(swResults,hwResultsFirst,bcidHWnew);

size(hwResults,1)
size(hwResultsFirst,1)
size(swResultsPass,1)
nEvents = min(size(hwResultsFirst,1),size(swResultsPass,1))
%disp('SW strips')
%swResultsPass(:,1:8)'
%disp('HW strips')
%hwResults(:,1:8)'
%disp('stripsDiff')
%[swResultsPass(:,1:8)' - hwResults(1,1:8)']
%disp('m strips hw')
%hwResults(1,15:22)
%disp('m strips sw')
%swResultsPass(:,21:28)'
%disp('m strips diff')
%[hwResults(1,15:22)' - swResultsPass(:,21:28)']
%disp('m global')
%swResultsPass(:,9)' 
%hwResults(:,9)'
%disp('m global diff')
%[swResultsPass(:,9)' - hwResults(:,9)']
%disp('m x local')
%swResultsPass(:,10)'
%hwResults(:,12)'
%disp('m x local diff')
%[swResultsPass(:,10)' - hwResults(:,12)']
%disp('cart')
%swResultsPass(:,[11 12])'
%hwResults(:,[13 14])'
%disp('BCIDS')
%bcidSW
%bcidHW
size(swResultsFail)
size(swResultsPass)
size(hwResultsFirst)
bcidSW(:,1)
bcidHWnew
bcidHW

histogram(swResultsPass(:,10)-hwResults(:,12))
for i=1:nEvents
    if abs(swResultsPass(i,10)-hwResults(i,12))>0.02
        disp('problem in m local')
        %i
        swResultsPass(i,29)
        swResultsPass(i,10)
        hwResults(i,12)
        %bcidSW(swResultsPass(i,29),:)
        hwResults(i,15:22)' 
        swResultsPass(i,21:28)'
        hwResults(i,:)
    end
end


%swResults(:,13:20)
%bcidHW
%swResultsFail(:,29)

%two problems: sometimes double (one bit getting lost somewhere?, sometimes
%just off by quite a bit (adds next bunch crossing, and divides by 3 rather than 4)
% $$$disp('results')
% $$$swResults(1:printLimit,[1 9:15])
% $$$hwResults(1:printLimit,[1 9:15])

% $$$ removeIndex = 1;
% $$$ while size(swResults,1) > nEvents ...
% $$$         && removeIndex < nEvents;
% $$$  
% $$$    quality = sum(swResults(removeIndex,1) ...
% $$$              - (hwResults(removeIndex,1)));
% $$$  
% $$$    if quality > 0
% $$$        disp('event removed')
% $$$        removeIndex
% $$$        swResults(removeIndex,:) = [];
% $$$    else
% $$$       removeIndex = removeIndex +1;      
% $$$    end
% $$$ end
% $$$ 
% $$$ disp('results alligned')
% $$$ swpl = swResults(1:printLimit,[1 9:15])
% $$$ hwpl = hwResults(1:printLimit,[1 9:15])
% $$$ 
% $$$ swResults = swResults(1:nEvents,:);
% $$$ 
% $$$ residules = swResults(:,9:15) -abs(hwResults(:,9:15));
% $$$ 
% $$$ residules(1:printLimit,:)
% $$$ percentError = (residules ./ swResults(:,9:15)) * 100 ;
% $$$ 
% $$$ figure(10)
% $$$ plot(swResults(:,1), percentError(:,5),'x');
% $$$ figure(11)
% $$$ plot(swResults(:,1), percentError(:,6),'x');
% $$$ figure(12)
% $$$ plot(swResults(:,1), percentError(:,7),'x');
% $$$ 
% $$$ figure(2)
% $$$ missing = swResults(:,1:8) -  hwResults(:,1:8);
% $$$ missing = reshape(missing,[],1);
% $$$ missing(find(missing == 0)) = [];
% $$$ hist(missing,1000);
% $$$ 
% $$$ figure(4)
% $$$ hwResM = hwResults(:,9:16);
% $$$ hist(hwResM(1:end),100);
% $$$ mean(hwResM,2);
% $$$ 
% $$$ figure(5)
% $$$ plot(mean(hwResM,2), percentError(:,5),'*');
