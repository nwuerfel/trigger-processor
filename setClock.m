function[DataReceived] =  setClock(tcpObj)
	header = 'abcd1234';
tcpObj = tcpOpenMmtp 
type = 'FE170102';
	addr = '40800074';
        data = '00000008';
	tcpPayload = hex2dec([header;type;addr;data])  ;
	fwrite(tcpObj,tcpPayload, 'uint32');
	timeout = 20;
	timeWait= 0;

	while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
	    pause(.1)
	    timeWait = timeWait+1;
	end

	while(get(tcpObj, 'BytesAvailable') > 0)
	    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4, ...
	                                 'uint32'));
	end
    tcpCloseMmtp(tcpObj);
    end