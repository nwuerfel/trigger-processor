function[worse] = findWorse(difs, threshold, rigs)
	worse = [];
	
	for k = 1:size(difs,1)
		if(abs(difs(k,2)) > threshold)
			if(find(rigs(1,:)==difs(k,1)))
				worse = [worse, difs(k,1)];
			end
		end
	end

end