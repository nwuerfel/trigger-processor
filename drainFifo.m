function[DataReceived] =  drainFifo(tcpObj,addr);
header = 'abcd1234';
type = 'FE170011';
data = '00000001'; %start DAQ;
tcpPayload = hex2dec([header;type;addr;data]);;
fwrite(tcpObj,tcpPayload, 'uint32');
timeout = 4000; %20;
timeWait= 0;

while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout);
    pause(.1);
    timeWait = timeWait+1;;
end

while(get(tcpObj, 'BytesAvailable') > 0)
    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4,'uint32'));
end