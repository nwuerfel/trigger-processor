function noOfBits(hits, extraPerHit)
	if(~exist('extraPerHit', 'var'))
		extraPerHit = 0;
	end
	hitdataBits = 20;
	bxidBits = 12;
	width = 32;
	totalBits = bxidBits + hits*(hitdataBits + extraPerHit)
	words = ceil((bxidBits + hits*(hitdataBits + extraPerHit))/width)
	leftover = words*width - totalBits
end