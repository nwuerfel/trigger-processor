function[DataReceived] =  stopDaq(tcpObj)
header = 'abcd1234';
type = 'FE170002';
addr = '00000001'; %command address;
data = '00000011'; %stop DAQ;
tcpPayload = hex2dec([header;type;addr;data]);
fwrite(tcpObj,tcpPayload, 'uint32');
pause(.1)


%timeout = 20;
%timeWait= 0;
%
%while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
%    pause(.1);
%    timeWait = timeWait+1;;
%end
%
%while(get(tcpObj, 'BytesAvailable') > 0)
%    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4,'uint32'));
%end
