function testEventsBetween(firstEv, lastEv)
	tic
	load('nathan.mat');

	load('testOutput');

	% noRes = [];
	% overRes = [];
	% sizeMismatch = [];
	% resList = [];

	% testCount = 0;
	alignFails = [];
	for ev = firstEv:lastEv
		if(numel(find(Hits(:,1) == ev)) > 0)
			testCount = testCount + 1;
			[resCount, hardRes, simRes, rl] = runForEvents(ev, ev);
			if(resCount <= 1)
				noRes = [noRes, ev];
			elseif(resCount > 18)
				overRes = [overRes; ev, resCount];
			elseif (size(hardRes) ~= size(simRes))
				sizeMismatch = [sizeMismatch; ev, size(hardRes,1), size(simRes,1)];
			else
				resList = [resList; ev, hardRes, simRes];
				if(isempty(rl))
					alignFails = [alignFails, ev];
				end
			end
		end
	end
	noResTotal = size(noRes,1);
	overResTotal = size(overRes,1);
	sizeMismatchTotal = size(sizeMismatch,1);
	withResTotal = size(resList,1);

	testCount
	noResTotal
	overResTotal
	sizeMismatchTotal
	withResTotal
	noRes
	overRes
	sizeMismatch
	alignFails

	save('testOutput.mat', 'testCount','noRes','overRes', 'sizeMismatch', 'resList', '-append');
	toc
end
