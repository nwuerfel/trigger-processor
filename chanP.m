runLn = 100000
takeMax = 5
fid = fopen('./chanP.dat');
chanProb = fscanf(fid, '%f\n') * .01;
chanHitSum = zeros(length(chanProb),1);
chanHitSumLimit = zeros(length(chanProb),takeMax);
for i = 1:runLn
    chanHit = chanProb > rand(length(chanProb),1);
    chanHitAcum = cumsum(chanHit);
    chanHitSum = chanHitSum + chanHit;
    for take = 1:takeMax
        takeLast = find (chanHitAcum == take,1,'first');
        chanHitSkip = chanHit;
        chanHitSkip(takeLast + 1 :end) = 0;
        chanHitSumLimit(:,take) = chanHitSumLimit(:,take)+chanHitSkip;
    end
end
chanHit
effic = chanHitSumLimit./repmat(chanHitSum,1,takeMax)
chanHitSum = chanHitSum / runLn
fclose(fid);