%
% calculate C_i for:
% mx_local = sum( C_i * y_i )
%

zbases      = [7583.5  7594.5  7752.5   7763.5];
strip_pitch = 0.445;
nplanes     = size(zbases, 2);
nconfigs    = power(2, nplanes);

word_length = 32;
frac_length = 39;

fprintf('\n');
fprintf('-- WORD -- plane config (4 bits) & plane (2 bits). ');
fprintf('fraction: %i \n\n', frac_length);

for config = nconfigs-1 : -1 : 0
    
    configuration = fliplr(de2bi(config, 4)); % e.g., 13 = 1101
    zbasesSel     = zbases;
    
    for j=1:size(zbasesSel, 2)
        zbasesSel(j) = zbasesSel(j)*configuration(j);
    end

    sum_zbasessq  = zbasesSel * zbasesSel';
    sum_zbases_sq = sum(zbasesSel) * sum(zbasesSel);

    A    = sum(configuration) / ... 
            (sum(configuration)*sum_zbasessq - sum_zbases_sq);
    B    = A * sum(zbasesSel) / sum(configuration);
    zavg =     sum(zbasesSel) / sum(configuration);

    for plane=nplanes:-1:1

        % skip these plane configs
        if isequal(configuration, [1 1 0 0]) || ...
           isequal(configuration, [0 0 1 1]) || ...
           sum(configuration) == 1           || ...
           sum(configuration) == 0 
            C = 0;

        % keep these plane configs
        else
            C = B * (zbases(plane)/zavg - 1) * strip_pitch;
        end

        C_sfi = sfi(C, word_length, frac_length);

        fprintf('%s -- %i%i%i%i%s  (%12.9f)\n', ...
                upper(C_sfi.hex),                      ...
                configuration(1), configuration(2),    ...
                configuration(3), configuration(4),    ...
                dec2bin(plane-1, 2), C);
        
        Cs = [Cs C];
    end
end

% uncomment me to see the recommended FractionLength
% sfi(Cs, 32)
