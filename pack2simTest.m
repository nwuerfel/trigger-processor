%% pack2sim: given array of packet data ([bxid layer side packetData]), produce simulation input
%% optional second argument: outfile - full filename of file to produce
function pack2simTest(bcid,vmmIDs,artData, outfile)
	if(~exist('outfile', 'var'))
		outfile = 'packetDataIn_v1.txt'; 
	end
% packet = [nOfHits, bxid, vmmIDs(x8), artDataParity, artData(x8)]
        nOfHits = size(vmmIDs,1)
        artDataParity = 0
        vmmIDs = [hex2dec(vmmIDs)', zeros(1,8 - nOfHits)]
        artData = [hex2dec(artData)', zeros(1,8 - nOfHits)]
 bcid = hex2dec(bcid)
 
 %the first 3 values are set to 0 
 %these are fiber numbers 
 packet = [0,0,0,nOfHits, bcid, vmmIDs, artDataParity, artData]
  pack2sim(packet, outfile)      
end