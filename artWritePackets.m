%% artWritePackets: extract sequence of 32-bit words from a packet input file
%% and write them to the fifo via tcp;
%% a new tcp packet is sent for each particular input fifo
function [] = artWritePackets(tcpObj,inFile)
    header = 'abcd1234';
    type = 'FE170002';
    maxChunksPerWrite = 100; % limit the number of chunks sent in one tcp packet

    [hexChunks, hexAddr] = textread(inFile, '%s %s');
    chunks = hex2dec(hexChunks);
    numAddr = hex2dec(hexAddr);
    for fifo = 32:63 % offset by 32 to put in correct address space
        chunksToSend = chunks(find(numAddr(:,1) == fifo));
        if(~isempty(chunksToSend))
          addr = ['000000',dec2hex(fifo,2)];
            finalChunk = size(chunksToSend,1);
            lastChunk = 0;
            while(lastChunk < finalChunk)
                prevChunk = lastChunk+1;
                lastChunk = min(finalChunk, lastChunk+maxChunksPerWrite);
                disp(sprintf('  Writing 32-bits ART word %d-%d to fifo %s : ', ...
                             [prevChunk, lastChunk, dec2hex(fifo)]));
                tcpPayload = [hex2dec([header;type;addr]);chunksToSend(prevChunk:lastChunk,:)];
                hexPayload = dec2hex(tcpPayload,8);
                fwrite(tcpObj, tcpPayload, 'uint32');

                %% wait a while to allow the tcp packet to be received
                pause(.3);
            end			
        end
    end
    
end

