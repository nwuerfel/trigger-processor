%% mat2csv: given array of Hits, produce the file 'hitData.csv' with input
%% appropriate for 'doIt'
function mat2csv(matArr)
	outfile = 'hitData.csv'

	Hits = matArr; startRow = 1; endRow = size(matArr,1);
	dlmwrite(outfile,Hits(startRow:endRow,[1 5 6 10 24 25 26 27 28 29]))
	type(outfile)
end

