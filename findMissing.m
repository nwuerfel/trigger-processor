%% findMissing: given two reslists, produce array of [event numbers, flag] where
%% flag is '0' if an event is present in the first reslist but not in the second, and 
%% is '1' otherwise
function[dif] = findMissing(oldReslist, newReslist)
	dif = [];
	for k = 1:numel(oldReslist(:,1))
		dif = [dif; oldReslist(k,1), ~isempty(find(newReslist(:,1)==oldReslist(k,1)))];
	end
	totalMissing = size(dif,1) - sum(dif(:,2));
	totalMissing
end
