files = ['hitsAndResults10.mat '; 'hitsAndResults20.mat '; 'hitsAndResults30.mat ';...
    'hitsAndResults50.mat '; 'hitsAndResults100.mat'; 'hitsAndResults200.mat'];

lowestXstation1 = 1000;
lowestXstation2 = 1000;
highestXstation1 = 1000;
highestXstation2 = 1000;

lowestUVstation1 = 1000;
lowestUVstation2 = 1000;
highestUVstation1 = 1000;
highestUVstation2 = 1000;

for n = 1:size(files,1)
    load(strtrim(files(n,:)));
    tempLowestXstation1 = findLowestStrip(Hits,1,1,0);
    tempLowestXstation2 = findLowestStrip(Hits,2,1,0);
    tempHighestXstation1 = findLowestStrip(Hits,1,0,0);
    tempHighestXstation2 = findLowestStrip(Hits,2,0,0);

    tempLowestUVstation1 = findLowestStrip(Hits,1,1,1);
    tempLowestUVstation2 = findLowestStrip(Hits,2,1,1);
    tempHighestUVstation1 = findLowestStrip(Hits,1,0,1);
    tempHighestUVstation2 = findLowestStrip(Hits,2,0,1);
    
    if (tempLowestXstation1<lowestXstation1)
        lowestXstation1=tempLowestXstation1;
    end
    if (tempLowestXstation2<lowestXstation2)
        lowestXstation2=tempLowestXstation2;
    end
    if (tempHighestXstation1>highestXstation1)
        highestXstation1=tempHighestXstation1;
    end
    if (tempHighestXstation2>highestXstation2)
        highestXstation2=tempHighestXstation2;
    end
    
    if (tempLowestUVstation1<lowestUVstation1)
        lowestUVstation1=tempLowestUVstation1;
    end
    if (tempLowestUVstation2<lowestUVstation2)
        lowestUVstation2=tempLowestUVstation2;
    end
    if (tempHighestUVstation1>highestUVstation1)
        highestUVstation1=tempHighestUVstation1;
    end
    if (tempHighestUVstation2>highestUVstation2)
        highestUVstation2=tempHighestUVstation2;
    end
    clearvars -except lowestXstation1 lowestXstation2 highestXstation1...
        highestXstation2 lowestUVstation1 lowestUVstation2 highestUVstation1...
        highestUVstation2 files n;
end