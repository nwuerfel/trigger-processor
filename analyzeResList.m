%% analyzeReslist: compute and return differences between simulation 
%% and harware output contained in 'reslist';
%% if the second argument 'plot' is set and true, produce plots of results ('1' by default);
function[Difs] = analyzeResList(resList, plot)
if ~exist('plot','var')	plot = 1; end

% Convert unsigned columns to signed

resList(:, 27) = abs(resList(:, 27)); % hard. mx
resList(:, 20) = resList(:, 20); % hard. my / hard. mx global
resList(:, 18) = resList(:, 18); % hard. mv global
resList(:, 19) = resList(:, 19); % hard. mu global
resList(:, 21) = resList(:, 21); % hard. mx local

% Reference for column numbers

resList(:, 34); % sim. mx global
resList(:, 35); % sim. mu global
resList(:, 36); % sim. mv global
resList(:, 37); % sim. mx local
resList(:, 38); % sim. mx
resList(:, 39); % sim. my

evCol = resList(:,1); % event number column

% Compute differences (hard. - sim.)

mx_difs 	    =  [resList(:,27) - resList(:,38)];
my_difs         =  [resList(:,20) - resList(:,39)];
mx_global_difs  =  [resList(:,20) - resList(:,34)];
mu_global_difs  =  [resList(:,19) - resList(:,35)];
mv_global_difs  =  [resList(:,18) - resList(:,36)];
mx_local_difs   =  [resList(:,21) - resList(:,37)];

Difs = [evCol, mx_difs, my_difs, mx_global_difs, mu_global_difs, mv_global_difs, mx_local_difs];
nOfDifs = size(Difs, 1);

% Plots and histograms of differences

if(plot)
	plotDifference(resList(:,27), mx_difs, 'mx');
	drawHistogram(mx_difs,'mx');

	plotDifference(resList(:,20), my_difs, 'my');
	drawHistogram(my_difs,'my');

	plotDifference(resList(:,20), mx_global_difs, 'mx global');
	drawHistogram(mx_global_difs, 'mx global');

	plotDifference(resList(:,19), mu_global_difs, 'mu global');
	drawHistogram(mu_global_difs, 'mu global');

	plotDifference(resList(:,18), mv_global_difs, 'mv global');
	drawHistogram(mv_global_difs, 'mv global');

	plotDifference(resList(:,21), mx_local_difs, 'mx local');
	drawHistogram(mx_local_difs, 'mx local');
end

% nDifs = Difs;

% save('Difs.mat', 'Difs');

end