function[row] = firstRowForEvent(eventsColumn, eventNo)
	bound = find(eventsColumn < eventNo, 1, 'last');
	if(isempty(bound))
		row = 1;
	else
		row = min(bound+1, numel(eventsColumn));
	end
	% row = max(1, find(eventsColumn < eventNo, 1, 'last') + 1);
	% eventsColumn'
	disp(sprintf('Finding FIRST row for ev no. %d: %d',[eventNo,row]));
end
