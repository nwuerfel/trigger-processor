function [counts] = countHits(events)
	if(~isvector(events))
		disp('Bad input');
		return
	end
	if(~iscolumn(events)) events = events'; end

	counts = [events, arrayfun(@(ev) size(hitsForEvents(ev),1),events)];
end
