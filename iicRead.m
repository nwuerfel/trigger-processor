function[DataReceived] =  iicRead(tcpObj,addr)
header = 'abcd1234';
type = 'FE170101';
tcpPayload = hex2dec([header;type;addr])  ;
fwrite(tcpObj,tcpPayload, 'uint32');
timeout = 5;
timeWait= 0;

while ((get(tcpObj, 'BytesAvailable') == 0) && timeWait < timeout)
    pause(.1)
    timeWait = timeWait+1;
end

while(get(tcpObj, 'BytesAvailable') > 0)
    DataReceived = dec2hex(fread(tcpObj,tcpObj.BytesAvailable/4, ...
                                 'uint32'));
end
end