load('Difs2.mat');

% Columns:
% Difs = [evCol, mx_difs, my_difs, mx_global_difs, mu_global_difs, mv_global_difs, mx_local_difs];
% 	        1       2        3           4               5                6              7

sortedByMx       = flipdim(sortrows(Difs, 2),1);
sortedByMy       = flipdim(sortrows(Difs, 3),1);
sortedByMxGlobal = flipdim(sortrows(Difs, 4),1);
sortedByMuGlobal = flipdim(sortrows(Difs, 5),1);
sortebByMvGlobal = flipdim(sortrows(Difs, 6),1);
sortedByMxLocal  = flipdim(sortrows(Difs, 7),1);

SortedEvents = [sortedByMx(:,1), sortedByMy(:,1), sortedByMxGlobal(:,1),...
				sortedByMuGlobal(:,1), sortebByMvGlobal(:,1), sortedByMxLocal(:,1)];

WorstTenEvents = SortedEvents(1:10, :)

save('SortedByDifs2.mat', 'SortedEvents', 'WorstTenEvents');
