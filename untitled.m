%UNTITLED Code for communicating with an instrument.
%
%   This is the machine generated representation of an instrument control
%   session. The instrument control session comprises all the steps you are
%   likely to take when communicating with your instrument. These steps are:
%   
%       1. Create an instrument object
%       2. Connect to the instrument
%       3. Configure properties
%       4. Write and read data
%       5. Disconnect from the instrument
% 
%   To run the instrument control session, type the name of the file,
%   untitled, at the MATLAB command prompt.
% 
%   The file, UNTITLED.M must be on your MATLAB PATH. For additional information 
%   on setting your MATLAB PATH, type 'help addpath' at the MATLAB command 
%   prompt.
% 
%   Example:
%       untitled;
% 
%   See also SERIAL, GPIB, TCPIP, UDP, VISA, BLUETOOTH.
% 
 
%   Creation time: 13-Mar-2014 15:19:15

% Find a tcpip object.
obj1 = instrfind('Type', 'tcpip', 'RemoteHost', '128.103.100.175', 'RemotePort', 7, 'Tag', '');

% Create the tcpip object if it does not exist
% otherwise use the object that was found.
if isempty(obj1)
    obj1 = tcpip('128.103.100.175', 7);
else
    fclose(obj1);
    obj1 = obj1(1)
end

% Connect to instrument object, obj1.
fopen(obj1);

% Communicating with instrument object, obj1.
fwrite(obj1, 3450549266, 'uint32');
fwrite(obj1, 3450549266, 'uint32');
fprintf(obj1, '3450549266');
data1 = fscanf(obj1);
fwrite(obj1, 3450549266, 'uint32');
fprintf(obj1, '3450549266');
data2 = fscanf(obj1);

% Disconnect all objects.
fclose(obj1);

% Clean up all objects.
delete(obj1);

