function Data=newUdp(ipA,portA,ipB,portB)
% Create UDP Object
echoudp('on',portA);
udpB = udp(ipA,portA,'LocalPort',portB);
set(u, 'OutputBufferSize', 1024);
% Connect to UDP Object
fopen(udpB);
set(u, 'DatagramTerminateMode', 'off');
Data=fscanf(udpB);
Data = fread(udpB, 250, 'int32');
% Clean Up Machine B
fclose(udpB);
delete(udpB);
clear ipA portA ipB portB udpB
echoudp('off')
end