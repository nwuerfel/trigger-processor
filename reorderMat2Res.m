function[reorderedRes] = reorderMat2Res(events, results)
	reorderedRes = [];
	for n = 1:numel(events)
		reorderedRes = [reorderedRes; results(find(results(:,1)==events(n)),:)];
	end
end
