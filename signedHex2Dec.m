function[dec] = signedHex2Dec(hex)
	dec = double(typecast(uint16(hex2dec(hex)),'int16'));
end