README - state of the matlab side of things - by Piotr Galuszka 
(pgaluszka@college.harvard.edu)

This readme reflects the changes that were brought about by the transition 
to gbt packets as input for the trigger processor.

Base Hit data
-------------

The base data for hits comes from Brian and is contained in the array Hits in 
the file "nathan.mat"; those are usually referrred to as Hits with capital 'H' 
in my comments.

These Hits are grouped by event number:
'hitsForEvents' is a function which takes a vector of event numbers and 
returns the relevant Hits in original order.

Getting simulation results
--------------------------

Simulation results for events are based off the values provided by Brian
in Hits:
'mat2res' is a function that takes an array of Hits and produces an array of 
simulation results.
 
Gbt packets
-----------

The Hits can be turned into Packets:
'packetsFromEvents' is a function which similarly takes a vector of event 
numbers and returns an array of packets, where each row has the form
[bxid, layer, side, packetData], and packetData contains the fields which
actually form the packet contents, i.e. 
[# of hits, nOfHits, bxid, vmmIDs, artDataParity, artData]

(Internally there is a number of functions used:
'assemblePackets' is the master function that takes the Hits to Packets and
returns them as a deep cell array; and 'flattenPackets' "flattens" it to the 
format given above.
'extractLVA' is the function which computes the relevant values: layer, vmm id
and art data for a given hit.)

'pack2sim' is a function that takes such a (flat) packet array and produces
an input file that can be used by the hardware simulation or as input to be sent
to the hardware via the function 'artWritePackets'.

(Internally the actual gbt packet format is determined in the function
'packetToBinary', and 'pack2sim' splits it into chunks and adds on information
about order and region to allow the packets to be addressed properly and sent as
32-bit words')

Getting hardware results
------------------------

'doPackets' is a function that, given a packet input file, opens a tcp 
connection to the hardware, writes the packets to the fifo, runs the hardware,
and returns the hexadecimal output.

(Internally:

'artWritePackets' is a function that takes a packet input file (as produced by
'pack2sim' and a tcp object, and writes the packets to the fifo in the hardware.
It sends a new tcp packet for each new "address", i.e. fiber, and also possibly
sends more than one packet if there are too many chunks.

'formatTPDebug' is a function that takes the raw hex hardware output and returns
an array of hardware results in decimal representation.)

Putting hardware and simulation together
----------------------------------------

The top level function to run data through the hardware is 'runForEvents', which
takes a vector of event numbers. It selects the relevant rows from Brian's file,
splits them into subsets that fit in the fifo, writes them to the hardware, runs
the hardware, extracts the simulation results, and aligns them with hardware 
output.

Internally, 'alignResults' is a function that takes the Hits used, the hardware 
output and the simulation results for them and produces an array of aligned 
results in the form: [event number, hardware results, simulation results]; such
an array is referred to as a "reslist" throughout.

Analyzing correctness
---------------------

'analyzeResList' is a function that takes a "reslist" (as returned by
'alignResults') and produces an array of differences between relevant hardware
and simulation fields; it also (optionally) produces plots of those differences.
This function also logs its workings in the file 'log'; this is because
originally it wrote a lot to standard output.

'sortDifs' takes an array of differences as returned by 'analyzeResList' and
produces arrays of event-difference pairs sorted by difference for each
relevant field.

Miscellaneous
-------------

There are a few helpful functions worth pointing out:

'rehex' and 'unhex' can turn a decimal to a hex (including shifting the value 
and incorporating sign -- with a flag) and vice versa for e.g. quickly verifying
values seen in the hardware simulation.

'rigMx' is a function that was used to show that the sign errors were really
happening; it can take a "reslist" and artificially attempt to correct the 
most significant bit of a field (hardcoded to be mx but easy to change) if
such an operation improves the agreement between hardware and simulation.

'findMissing' can be used to quickly compare two "reslists" for differences in 
the events they contain.