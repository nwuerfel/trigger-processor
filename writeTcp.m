function writeTcp(addr,data);
% Ref. http://www.mathworks.com/help/instrument/writing-and-reading-data_f16-57447.html
% Writing and Reading Data with a TCP/IP Object


header = 'abcd1234'
t = tcpip('128.103.100.175', 7); 
% Set size of receiving buffer, if needed. 
set(t, 'InputBufferSize', 30000); 
fopen(t);
% get data from file
%fid = fopen('sendFile.txt','r');                                     %Open the data file
%writeData = fscanf(fid,'%x');
%fwrite(t,(writeData) , 'uint32');
fwrite(t,hex2dec([header;addr;data]) , 'uint32');
timeout = 20;

timeWait= 0;


while ((get(t, 'BytesAvailable') == 0) && timeWait < timeout)

    pause(.1)
        timeWait = timeWait+1;
end
while(get(t, 'BytesAvailable') > 0)
    DataReceived = dec2hex(fread(t,t.BytesAvailable/4,'uint32'))
end

% Disconnect and clean up the server connection. 
fclose(t); 
delete(t); 
clear t 
