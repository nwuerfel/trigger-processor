function testEventsGroupedBetween(firstEv, lastEv)
	load('nathan.mat');

	load('testGroupedOutput');

	% noRes = [];
	% overRes = [];
	% sizeMismatch = [];
	% resList = [];

	uniqueEvents = unique(Hits(:,1));
	realEvents = intersect([firstEv:lastEv],uniqueEvents');

	maxEvsInGroup = 10;

	lastSentInd = 1;
	while(lastSentInd < numel(realEvents))
		nextLastInd = min(numel(realEvents), lastSentInd+maxEvsInGroup);
		
		[resCount, hardRes, simRes] = runForEvents(realEvents(lastSentInd), realEvents(nextLastInd));
		numberOfEventsSent = (nextLastInd - lastSentInd + 1);
		
		testCount = testCount + numberOfEventsSent;
		if(resCount ~= 1+17*numberOfEventsSent)
			disp(sprintf('Bad response on packet %d-%d',...
				[realEvents(lastSentInd), realEvents(nextLastInd)]));
		elseif (size(hardRes) ~= size(simRes))
			disp(sprintf('Size mismatch on packet %d-%d',...
				[realEvents(lastSentInd), realEvents(nextLastInd)]));
		else
			resList = [resList;...
				range(realEvents(lastSentInd), realEvents(nextLastInd))', hardRes, simRes];
		end
		
		lastSentInd = nextLastInd;
	end

	% noResTotal = size(noRes,1);
	% overResTotal = size(overRes,1);
	% sizeMismatchTotal = size(sizeMismatch,1);
	% withResTotal = size(resList,1);

	% testCount
	% noResTotal
	% overResTotal
	% sizeMismatchTotal
	% withResTotal
	% noRes
	% overRes
	% sizeMismatch

	% save('testGroupedOutput.mat', 'testCount','noRes','overRes', 'sizeMismatch', 'resList', '-append');
end
