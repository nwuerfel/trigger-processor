function array = JUDPplot_s(brdSel)
%Code for sending and receiving hex .txt files over UDP.
%to send use   JUDPtxt('send',5001,'128.103.100.174','file.txt')
%
%
%JUDPtxt('send',PORT,ADDR,FILE) sends a file to the specified port and address. 
%
% Adapted from JUDP.m:
% FILE = JUDPtxt('RECEIVE',PORT,PACKETLENGTH, FILE) reconstructs a file from the
% specified port. PACKETLENGTH should be set to the maximum expected
% message length to be received in the UDP packet; if too small, the
% message will be truncated. 
%
% FILE = JUDPtxt('RECEIVE',PORT,PACKETLENGTH, FILE, TIMEOUT) attempts to receive a
% file but times out after TIMEOUT milliseconds. If TIMEOUT is not
% specified, as in the previous example, a default value is used.
%
%Sends text documents to int8 as a UDP packet.
% Developed in Matlab 7.12.0.635 (R2011a) on maci64.
% Hilton Simmet (hsimmet@college.harvard.edu), 20120621 14:48
%   with guidance from Nathan Felt and use of JUDP.m by Kevin Bartlett 
%
%
SEND = 1;
RECEIVE = 2;
DEFAULT_TIMEOUT = 30000;
count = 0;
    port = 6006;
    packetLength = 1024;
    file = 'mmTest.txt';
    timeout = 30000;
    hitSum = zeros(64,1)';
    dlmwrite(file,[],'w');
    brdSel = str2num(brdSel)
    %Reconstruct .txt file from UDP
    while 1
        count = count + 1;
        mess = JUDP('receive',port,packetLength,timeout);
        C = dec2hex(typecast(mess, 'uint8'));
        array = cellstr((reshape(C',8,[]))');
        if length(array) > 3
            arrayBin = dec2bin(hex2dec(cell2mat(array)),32);
            chanNum = bin2dec(arrayBin(4:end,1:6));
            brdNum = bin2dec(arrayBin(2,25:32));
            disp (['packet ',num2str(count),' received from board ',num2str(brdNum),]);
            if brdNum == brdSel
                [chanNew,chanBins] = hist(chanNum,0:63);
                hitSum = hitSum + chanNew;
                if mod(count,5) == 0
                    figure(1)
                    bar(chanBins,hitSum,'r')
                    axis([0 63 0 max(hitSum)*1.1]);
                end
            end
        end
        %return;
end %if


