y = [3192; 3198; 3211; 3213; 3246; 3251; 3265; 3267]
%y = [5101;5111;5159;5106;5188;5195;5247;5193]
%y = [6229;6239;6267;6268;6332;6341;6370;6370]
%y = [6165;6174;6199;6192;6268;6277;6302;6295];
y = y 
load('mxLocalAB.mat')


z = [7478;7489;7521;7532;7604;7615;7647;7658];
zm = (1 ./ z) .* .445

A_local = A_local
sel_x = [1;2;5;6];

xy = y(sel_x)
xz = z(sel_x)


aszy = A_local(1) * sum(xy .* xz)
bsy = B_local(1) * sum(xy)
mxl = (aszy - bsy)

m = y .* zm
mxg = mean(m(sel_x))
mug = mean(m([3 7]))
mvg =  mean(m([4 8]))
%calc mx my for ROI
sa = 1.5
csc_sa = csc(2*pi * sa/360)
cot_sa = cot(2*pi * sa/360)

ROI_mx(1) = csc_sa * mug - cot_sa * mxg
ROI_mx(2) = cot_sa * mxg - csc_sa * mvg
ROI_mx_cart = mean(ROI_mx)

a_shift = 23
A_local_h = dec2hex(floor(A_local*2^a_shift))

b_shift = 10
B_local_h = dec2hex(floor(B_local*2^b_shift))

z_shift = 0
z_h = dec2hex(floor(z*2^z_shift))

zm_shift = 30
z_h = dec2hex(floor(zm*2^zm_shift))
