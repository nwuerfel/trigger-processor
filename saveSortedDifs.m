function [sorted] = saveSortedDifs(reslist)
	Difs = analyzeResList(reslist,0);
	sorted = sortDifs(Difs);
	filename = strcat('sortedDifs-',datestr(clock,'yymmdd-HHMM'),'.mat');
	save(filename, 'sorted', 'Difs', 'reslist');
end
