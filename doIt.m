%% Run the hit data from the file 'hitData.csv' through the hardware, and store
%% results in the array fifoData (the output also gets saved in 'hitData-Out.csv');

fifoData = doItF('hitData.csv');

% t = tcpOpenMmtp          
% inFile = 'hitData.csv';
% inData = dlmread(inFile);
% maxRowsPerWrite = 100;
% finalRow = size(inData,1);
% lastRow = 1;
% disp(sprintf('Total rows in hitData.csv: %d', finalRow));
% while(lastRow < finalRow)
% 	prevRow = lastRow;
% 	lastRow = min(finalRow, prevRow+maxRowsPerWrite);
% 	disp(sprintf('Writing rows %d-%d',[prevRow, lastRow]));
% 	artWriteRows(t, inData(prevRow:lastRow, :));
% end

% startDaq(t)                            
% fifoData = drainFifo(t,'00000020')

% dlmwrite('lastFifoOut', fifoData, '');
  
% status(t)
% tcpCloseMmtp(t)
