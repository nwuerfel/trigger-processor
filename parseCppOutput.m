filename='../../temp/mmt_hit_print_h0.0009_ctx2_ctuv1_uverr0.0035_setxxuvuvxx_ql1_qdlm1_qbg0_qt0_NOM_NOMPt100GeV.digi.ntuple.txt';
%regions for handling misalignments. No misalignments imply they are all the same
nzregions=8; 
startingBCID=20; %arbitrary BCID to start with
BCIDinterval=5; %BCID increase between events (a bit arbitrary)
fileID = fopen(filename,'r');
ybasesstr=textscan(fileID,'%s',3,'delimiter','\n');
%Station 1 x planes go from 65 to 5130
%Station 1 uv planes go from 73 to 5163    
%Station 2 x goes from 68 to 3110
%Station 2 uv goes from 110 to 3175
offsetBottom = [65 65 73 73 73 73 65 65];
offsetTop = [68 68 110 110 110 110 68 68];
ybasesBottomC=textscan(fileID,'%f %f %f %f %f %f %f %f',1,'delimiter','\n');
ybasesBottom = cell2mat(ybasesBottomC)+offsetBottom*0.445;
ybasesTopC=textscan(fileID,'%f %f %f %f %f %f %f %f',1,'delimiter','\n');
ybasesTop = cell2mat(ybasesTopC)+offsetTop*0.445;
zbasesstr=textscan(fileID,'%s',1,'delimiter','\n');
zbasesAllRegions=textscan(fileID,'%f %f %f %f %f %f %f %f',nzregions,'delimiter','\n');
BC_window=2;
background='bgoff';
headerFinal=textscan(fileID,'%s',1,'delimiter','\n');
%We have read in the configuration parameters of the algorithm
%Now define all the input and outputs for each event
%Column 1: event #; Column 2: BCID#; Column 3: plane#; Column 4: station#
%Column 5: strip#; Column 6: calculated slope
Hits = [];
%Column 1: true theta; Column 2: true phi; Column 3: true dtheta; Column 4:
%mxglobal; Column 5: myglobal; Column 6: mxlocal; Column 7: theta; Column
%8: phi; Column 9: dtheta
Event_Fit_Info = [];
%Now process each event
eventNumber = 1;
strDataFirst = '';
while(~feof(fileID))
    if(~strcmp(strDataFirst,'%Ev')) 
        evtHeader=textscan(fileID,'%s',1,'delimiter','\n'); %line starting with %Ev
    end
    strData=textscan(fileID,'%s',1,'delimiter','\n');
    strDataFirst=char(strData{1});
    strDataFirst=strDataFirst(1:3);
    while(~strcmp(strDataFirst,'---') & ~strcmp(strDataFirst,'%Ev'))
        %process hits
        data=cell2mat(textscan(char(strData{1}),'%f %f %f %f %f %f'));
        hitBCID=0;
        if(data(1)>75) 
            hitBCID=1;
        end
        plane = data(3);
        station= data(4);
        strip = data(5);
        slope = data(6);
        offset = offsetBottom(plane+1);
        if(station == 2)
            offset = offsetTop(plane+1);
        end
        thisHit = [eventNumber startingBCID+hitBCID plane station strip-offset slope];
        Hits = [ Hits; thisHit ];
        strData=textscan(fileID,'%s',1,'delimiter','\n');
        strDataFirst=char(strData{1});
        strDataFirst=strDataFirst(1:3);
        %finished processing event
    end
    %store event fit information
    if(strcmp(strDataFirst,'---'))        
        eventData=char(strData{1});
        initPar = findstr(eventData,'(');        
        endPar = findstr(eventData,')');
        Event_Fit_Info = [Event_Fit_Info; cell2mat(textscan(eventData(initPar(2)+1:endPar(2)-1),'%f, %f, %f, %f, %f, %f, %f, %f, %f'))];
    else
        Event_Fit_Info = [Event_Fit_Info; [-999 -999 -999 -999 -999 -999 -999 -999 -999]];
    end
    
    eventNumber=eventNumber+1;
    startingBCID = startingBCID+BCIDinterval;
end
%done processing events, now save the stuff we will need for the trigger
%processor simulation and for checking the results of that

save('hitsAndResults100.mat','filename','Hits','Event_Fit_Info','BC_window','background','nzregions',...
'ybasesBottom','ybasesTop','zbasesAllRegions');
    
