%% rehex: given a decimal number and the exponent by which to shift its value,
%% return the 4 most significant digits of its hexadecimal representation
function[hex] = rehex(decimal, shift)
	shifted = round(decimal.*2^(shift));
	fullHex = dec2hex(shifted,4);
	hex = fullHex(:,1:4);
end