%% given an unstable event, find preceding events which cause it to fail.
%% note: this takes a long time in general
function[results] = findCulprits(unstableEvent)
	resLisColToWatch = 21;
	load('nathan.mat');
	Events = unique(Hits(:,1));

	results = [];

	% establish base value
	[c,h,s,r] = runForEvents(unstableEvent,unstableEvent);
	results = [results; unstableEvent, r(find(r(:,1)==unstableEvent), resLisColToWatch)];
	thisIndex = find(Events(:,1)==unstableEvent);
	for k = 1:thisIndex-1
		[c,h,s,r] = runForEvents(Events(thisIndex-k),Events(thisIndex-k),unstableEvent,unstableEvent);
		val = r(find(r(:,1)==unstableEvent), resLisColToWatch);
		if(isempty(find(results(:,2)==val)))
			results = [results; unstableEvent-k, val];
		end
	end
end



