%% extractLVA: given values of plane and strip number,
%% compute Layer, Vmm id and ART
function [layer, vmmid, art] = extractLVA(plane, strip, station)
    %No offset needed. Strips start at 0 at the bottom of the station. 
    %There are two stations per plane
	ART_PER_VMM = 64;
	VMMS_PER_LAYER = 8;
	N_OF_LAYERS = 16;
	N_OF_PLANES = 4;
    N_OF_LAYERS_STATION_1 = 10;
    N_OF_LAYERS_STATION_2 = 6;
	rs = strip; % strip number relative to wedge
    if station ~= 1
        rs = rs + VMMS_PER_LAYER*ART_PER_VMM*N_OF_LAYERS_STATION_1;
    end
    
	layer = fix(rs/(ART_PER_VMM*VMMS_PER_LAYER));
    %max(rs);
	vmmid = fix(mod(rs,ART_PER_VMM*VMMS_PER_LAYER)/ART_PER_VMM) + mapPlane(plane)*VMMS_PER_LAYER;
	art = mod(mod(rs,ART_PER_VMM*VMMS_PER_LAYER),ART_PER_VMM);
	
	function [m] = mapPlane(plane)
		if(plane > 3);
			m = plane - 4;
		else
			m = plane;
		end
	end
end