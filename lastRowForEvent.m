function[row] = lastRowForEvent(eventsColumn, eventNo)
	bound = find(eventsColumn > eventNo, 1, 'first');
	if(isempty(bound))
		row = numel(eventsColumn);
	else
		row = max(1, bound-1);
	end
	% row = min(numel(eventsColumn), find(eventsColumn > eventNo, 1, 'first') -1);
	% eventsColumn'
	disp(sprintf('Finding LAST  row for ev no. %d: %d',[eventNo,row]));
end