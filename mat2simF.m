%% mat2simF: given array of Hits and a filename, produce a file with simulation input
%% named '<filename>-Sim.csv'
function mat2simF(matArr,outfile)
	outfile = strcat(outfile, '-Sim.csv');
	
	Hits = matArr;
	startRow = 1; endRow = size(Hits,1);
	Hits(startRow:endRow,[1 6 5]);
	nHits = (endRow - startRow) + 1;

	% simData =  [dec2hex(mod(Hits(startRow:endRow,1),16^4),4),repmat(' ',nHits,1), ...
	% 			dec2hex(mod((Hits(startRow:endRow,5)-1),16^1),1),repmat(' ',nHits,1), ...
	% 			dec2hex(mod(Hits(startRow:endRow,6),16^4),4),repmat(' ',nHits,1), ...
				% dec2hex(mod(Hits(startRow:endRow,10),16^2),2)];
	
	simData = [dec2hex(mod(Hits(startRow:endRow,10),2^12),3),repmat(' ',nHits,1),...
			   dec2hex(mod((Hits(startRow:endRow,5)-1),16^1),1), repmat(' ',nHits,1),...
			   dec2hex(mod(Hits(startRow:endRow,6),16^4),4)];

	fileID = fopen(outfile,'w');
	for n = 1:nHits
		fprintf(fileID,'%s\n',simData(n,:));
	end

	fclose(fileID);
	% type(outfile);
end