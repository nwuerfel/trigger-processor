nAddr = 1024;
outputFile = 'dthetaTable.coe'
header = [';******************************************************************\n',...
          ';********  DTheta Div to Mult Block Memory .COE file      *********\n',...
          ';******************************************************************\n',...
          '; This is used to convert a Div to Mult in the DTheta calc ,\n', ...
          '; For Single Port Block Memory v3.0 or later\n',...
          '; note: first data value is forced to 0000\n',...
          'memory_initialization_radix=16;\n',...
          'memory_initialization_vector=\n']

address = linspace(0,(2-(2/nAddr)),nAddr)'
data = floor((1 ./ (1 + address))* 2^16)
%since data(1) is the only member that requires 17 bits
%set it to zero.  can address 0 happen?
data(1) = 0

fileID = fopen(outputFile,'w');
fprintf(fileID,header);
fclose(fileID);


fileData = [dec2hex(data,4),repmat(',',size(data,1),1)]
fileData(end) = ';';
dlmwrite(outputFile,fileData,'delimiter','','-append');




