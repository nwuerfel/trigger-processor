%% getBcForEvent: given an event number and an array of Hits, find
%% the bunch crossing id at which the hits corresponding to the event
%% will be read (i.e. the earliest of their bxids)
function[bc] = getBcForEvent(eventNo, Hits)
	inputBcs = Hits(find(Hits(:,1)==eventNo),10);
	bc = min(inputBcs);
end