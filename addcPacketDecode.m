function[nHits,bcid,vmmId,artData] = addcPacketDecode(packet)
showResults = 0;
packetBin = dec2bin(hex2dec(packet'),4)';
%index so LSB is '1'
%need to clear out form previous run
vmmId = [];
artData = [];
packetBin = fliplr(packetBin(:)');
artDataBin = reshape(packetBin(1:48),6,[])';

packetBin(57:88);
nHits = sum(packetBin(57:88));
hitIndex = find(packetBin(57:88) == '1')';
nHits = size(hitIndex,1);
vmmId = dec2hex(hitIndex-1,2);
artData = dec2hex(bin2dec(fliplr(artDataBin)),2);
artData = artData(1:nHits,:);
bcid = dec2hex(bin2dec(fliplr(packetBin(97:108))),3);
errorFlags = packetBin(89:96);
if showResults == 1
       
    disp(['nHits :',num2str(nHits)])
    disp([repmat('vmmId :',nHits,1),vmmId,repmat(' artData :',nHits,1),artData ])
    disp(['bcid :',bcid])
    disp(['errorFlags :',errorFlags])
end

